Documentation du bot [DiscordLG.py](https://framagit.org/Powi/BotDiscordLG.py)
===
   * [Documentation du bot <a href="https://framagit.org/Powi/BotDiscordLG.py" rel="nofollow">DiscordLG.py</a>](#documentation-du-bot-discordlgpy)
   * [Déroulement d'une partie](#déroulement-dune-partie)
      * [Qui masterise ?](#qui-masterise-)
      * [Qui joue ?](#qui-joue-)
      * [Quels rôles sont en jeu ?](#quels-rôles-sont-en-jeu-)
         * [Les rôles](#les-rôles)
      * [C'est parti ?](#cest-parti-)
      * [La nuit <em>tous les chats sont gris</em>](#la-nuit-tous-les-chats-sont-gris)
         * [Cupidon](#cupidon)
         * [Salvateurice](#salvateurice)
         * [Voyant_e](#voyant_e)
         * [LG](#lg)
         * [Sorcier_e](#sorcier_e)
      * [Le jour se lève](#le-jour-se-lève)
         * [Les votes](#les-votes)
      * [Et ça continue](#et-ça-continue)
      * [Fin de partie](#fin-de-partie)
   * [Commandes](#commandes)
      * [Aides au mastering](#aides-au-mastering)
      * [Commandes MJ](#commandes-mj)
         * [Les rôles](#les-rôles-1)
      * [Inscription, désincription et spéctateurices](#inscription-désincription-et-spéctateurices)
      * [Commandes pour tout le monde](#commandes-pour-tout-le-monde)
      * [Commandes spécifiques à un rôle](#commandes-spécifiques-à-un-rôle)
      * [Code et Crédit](#code-et-crédit)
      * [Autre](#autre)
   * [Installer le bot sur son serveur.](#installer-le-bot-sur-son-serveur)
      * [Inviter le bot sur votre serveur](#inviter-le-bot-sur-votre-serveur)
      * [Les rôles](#les-rôles-2)
      * [Les salons](#les-salons)
         * [Permissions des salons](#permissions-des-salons)

# Déroulement d'une partie

## Qui masterise ?
Pour commencer, définir un·e MJ. Cette personne devra alors avoir le rôle Discord `MJ` et aucun·ne joueurse ne doit avoir ce même rôle (sinon c’est de la triche).

Quand læ MJ a le rôle, iel peut lancer la commande `!je suis mj` ou l'abrégée `!mj`. Le bot reconnaîtra donc cette personne comme MJ.

## Qui joue ?
Simultanément, les joueurses peuvent s'inscrire avec la commande `!inscription` ou l'abrégée `!ins`. 
Læ MJ reconnu·e par le bot peut également inscrire ces personnes avec la même commande, en mentionnant à la suite les personnes?:
```!inscription @joueur1#1234 @joueuse2#4321 @joeurse3#5678 @joueuse4#8765 …```
Ou abrégée :
```!ins @joueur1#1234 @joueuse2#4321 @joeurse3#5678 @joueuse4#8765 …```

## Quels rôles sont en jeu ?
Une fois tout le monde inscrit·e, læ MJ défini les rôles. 
Iel doit définir un nombre de rôles supérieur ou égal au nombre de joueurses. Oui oui, læ MJ peut définir plus de rôles que de joueurses.

**Mais alors que se passe-t-il me direz-vous ?**
Tout simplement le bot ne distribue pas tous les rôles.

**Mais… Et si il n'y a pas le Loups Garous ?**
Alors vous, MJ, jouerez leur rôle, comme s'il y avait des Loups Garous, c'est la variante **Peste noire**. Si les joueurses pensent qu'il n'y a pas de Loup et que vous êtes la **Peste noire** alors ils votent contre vous.
Si c'était effectivement vous, le village est sauvé. Sinon, libre à vous de leur faire subir toutes sortes de supplices pour avoir osé vous accuser à tort.

Ah oui, la commande pour définir les rôles, c'est `!roles role1, role2,role3,role4,…`

role1,role2,role3 et role4 étant les abréviations du nom des rôles (voir ci-après).

### Les rôles
Lors de la définition des rôles, læ MJ devra utiliser l'un·e des raccourci de nom de rôle que le bot connait, voici la liste?: 
* Roles.LoupGarou : "loup-garou","loup_garou","louve_garou","louve-garou","lg"
* Roles.SimpleVillageois : "simple-villageois","simple-villageoise","simple-villageois·e","sv"
* Roles.Voyant_e : "voyant","voyante","voyant·e","voyant_e","voyant-e","vovo","irma"
* Roles.Sorcier_e : "sorcière","sorcier","sorcerie","soso"
* Roles.Chasseureuse : "chasseur","chasseuse","chassou","chas"
* Roles.Cupidon : "cupidon","cupi","lover"
* Roles.Salvateurice : "salvateur","salvatrice","salva","salvateurice"
* Roles.Chaman : "shaman","shasha","chachou","chaman"
* Roles.InfectParentLoup : "père-des-loups","infect","infecte","infectparentloup","parentloup"
*?Roles.Servant_e : "servante","servant","servant_e","servant·e","dévouée","dévoué","devouee","devoue"
* Roles.JoueureuseDeFlute : "joueureuse","flûtiste","flutiste","fluflu"
* Roles.Ange : "ange","angel","lefourbe",
* Roles.Duadelphe : "duadelphe","duadelphes","du-adelphe","du-adelphes"
* Roles.Triadelphe : "triadelphe","triadelphes","tri-adelphe","tri-adelphes"
* Roles.Ancien_ne : "ancien","ancien·ne","ancien_ne","ancien.ne","ancienne"
* Roles.BoucEmissaire : "martyr","be","boucemissaire","bouc-emissaire","bouc_emissaire","bouc","souffre-douleur","cismec"
* Roles.Idiot_e : "idiot","idiote","idiot·e","idiot_e"

ex?: `!roles lg,vovo,soso,idiot` mettra les rôles LoupGarou,Voyant_e,Sorcier_e et Idiot_e dans la partie

## C'est parti ?
Et voilà, la partie peut commencer, MJ, utilisez la commande `!start` ! :smiley: 

## La nuit *tous les chats sont gris*
Une fois que le bot a lancé la partie (attendez bien son signal), c'est le moment de la première nuit, MJ, utilisez la commande `!nuit`
Vous devrez alors assister les joueurses pour chaque rôle.
Vous irez d'abord dans le salon `#duadelphe` pour faire jouer les duadelphe s'iels sont présent·es, puis irez au salon suivant pour faire jouer le rôle suivant. Les salons sont classés par ordre chronologique de réveil :smiley: 
Cependant, vous pouvez faire jouer plusieurs rôles simultanément, à quelques exceptions prêtes :
* Les rôles **après Cupidon** doivent jouer impérativement **après Cupidon** et **après** que les amoureuxes se soient reconnues.
* Personne ne peut jouer en même temps que les LG

### Cupidon

Pour désigner les amoureux, cupidon peut utiliser la commande `!amoureuxes surnom1 surnom2` ou son abréviation`!am surnom1 surnom2`

*Pour savoir les surnoms et qui est encore en jeu, utilisez la commande `!encore en vie` ou sa forme raccourcie `!eev`*

### Salvateurice

Pour désigner une personne à protéger, læ salvateurice peut utiliser la commande `!protéger surnom`

*Pour savoir les surnoms et qui est encore en jeu, utilisez la commande `!encore en vie` ou sa forme raccourcie `!eev`*

### Voyant_e

Pour désigner la personne à connaître le rôle, læ voyant_e peut utiliser la commande `!scan surmon`

*Pour savoir les surnoms et qui est encore en jeu, utilisez la commande `!encore en vie` ou sa forme raccourcie `!eev`*

### LG

Pour désigner la personne à manger, les LG peuvent utiliser la commande `!miam surmon`

*Pour savoir les surnoms et qui est encore en jeu, utilisez la commande `!encore en vie` ou sa forme raccourcie `!eev`*

### Sorcier_e

Dès que c’est son tour, la sorcière est informée par un message l'informant de qui doit mourir cette nuit. Si il lui reste sa potion de vie, le bot lui demandera si elle veut sauver la personne. Elle répond simplemnt par « oui » ou par « non » à la question.

Si elle veut utiliser sa potion de mort, elle utilise la commande `!potionMort surnom`

*Pour savoir les surnoms et qui est encore en jeu, utilisez la commande `!encore en vie` ou sa forme raccourcie `!eev`*

## Le jour se lève

MJ, vous pouvez annoncer le jour avec la commande `!jour` et annoncer les morts de la nuit avec la commande
`!kill surnom` ou `!kill @joueurse#1234`

### Les votes
Il est désormais temps de voter, les joueurses en vie peuvent voter grâce à la commande `!vote surnom` ou `!vote @joueures#1234`

Pour savoir les surnoms et qui est encore en jeu, utilisez la commande `!encore en vie` ou sa forme raccourcie `!eev`

Une fois les votes terminés, MJ, vous pouvez les arrêter avec `!stop votes`

En cas d'égalité et s'il n'y a pas de Bouc Émissaire, il peut y avoir un deuxième tour entre les personnes dans l'égalité.

## Et ça continue
La partie suit son court

## Fin de partie
Pour arrêter la partie, MJ, utilisez la commande `!stop`
Vous pouvez alors recommencer la partie ou tout arrêter avec `!splash`

# Commandes
## Aides au mastering
* "help": Permet d'obtenir une aide succinte du bot
* "masteriser":Pour obtenir de l'assistance pour masteriser une partie
* "ls","liste":Affiche la liste des rôles actuellement disponibles en jeu

## Commandes MJ
* "mj","je_suis_mj","je suis mj":Permet de s'annoncer comme MJ de la/des parties à venir.
* "roles","rôles":Permet aul MJ de définir les rôles présents dans la partie à venir. ex?: `!roles sv,lg,soso,vovo`
* "inscription","ins":Permet d'inscrire les joueurses mentionner à la/aux parties à venir.
* "lch","lancer","start":Pour lancer la partie
* "nuit":Pour déclancher la nuit dans le jeu
* "jour":Pour déclancher le jour dans le jeu
* "stp","arreter","arrêter","stop":Pour arrêter une partie
* "checkrole":Permet de connaître le rôle d'une personne (MJ uniquement)
* "kill","mort":Permet au MJ d'annoncer la mort d'une personne
* "stop_votes","stop_vote","stop votes","stop vote":Permet aul MJ de mettre un terme aux votes [Jour]
* "splash","reset":Pour remettre tout à zéro

### Les rôles
Lors de la définition des rôles, læ MJ devra utiliser l'un·e des raccourci de nom de rôle que le bot connait, voici la liste?: 
* Roles.LoupGarou?: "loup-garou","loup_garou","louve_garou","louve-garou","lg"
* Roles.SimpleVillageois?: "simple-villageois","simple-villageoise","simple-villageois·e","sv"
* Roles.Voyant_e?: "voyant","voyante","voyant·e","voyant_e","voyant-e","vovo","irma"
* Roles.Sorcier_e?: "sorcière","sorcier","sorcerie","soso"
* Roles.Chasseureuse?: "chasseur","chasseuse","chassou","chas"
* Roles.Cupidon?: "cupidon","cupi","lover"
* Roles.Salvateurice?: "salvateur","salvatrice","salva","salvateurice"
* Roles.Chaman?: "shaman","shasha","chachou","chaman"
* Roles.InfectParentLoup?: "père-des-loups","infect","infecte","infectparentloup","parentloup"
*?Roles.Servant_e?: "servante","servant","servant_e","servant·e","dévouée","dévoué","devouee","devoue"
* Roles.JoueureuseDeFlute?: "joueureuse","flûtiste","flutiste","fluflu"
* Roles.Ange?: "ange","angel","lefourbe",
* Roles.Duadelphe?: "duadelphe","duadelphes","du-adelphe","du-adelphes"
* Roles.Triadelphe?: "triadelphe","triadelphes","tri-adelphe","tri-adelphes"
* Roles.Ancien_ne?: "ancien","ancien·ne","ancien_ne","ancien.ne","ancienne"
* Roles.BoucEmissaire?: "martyr","be","boucemissaire","bouc-emissaire","bouc_emissaire","bouc","souffre-douleur","cismec"
* Roles.Idiot_e?: "idiot","idiote","idiot·e","idiot_e"

## Inscription, désincription et spéctateurices
* "spectateurice","specta":Permet à une personne d'avoir le rôle de Spectateurice et donc d'assister à une/plusieurs parties.
* "inscription","ins":Permet à un·e Joueureuse de s'inscrire.
* "des","desinscription","désinscription":Permet à un·e Joueureuse de se désinscrire

## Commandes pour tout le monde
* "eev","still_alive","encore_en_vie","still alive","encore en vie":Permet d'afficher la liste des joueurses encore en vie
* "vote","votes":Permet aux joueurses de voter [Jour] 

## Commandes spécifiques à un rôle
* "lovers","am","amoureuxes":Pour cupidon, lui permet de désigner les amoureuxes [première nuit]
* "scan":Pour læ Voyant_e, lui permet de connaître le rôle d'une pesronne [1/nuit]
* "miam":Pour les LG, leur permet de désigner une personne à manger [1/nuit]
* "potionmort":Pour læ Sorcier_e, lui permet d'utiliser sa potion de mort sur une personne [1/partie]
* "proteger","protéger":Pour læ salvateurice, lui permet de protéger une personne cette nuit [1/nuit]

## Code et Crédit

* "credit","crédit":Pour obtenir le crédit du bot
* "source","git","code":Pour obtenir le code source du bot

## Autre
* "wesh":Affiche le fameux message.
* "paslà":Invite à aller discuter d'un sujet ailleurs.

# Installer le bot sur son serveur.

## Inviter le bot sur votre serveur

[**lien**](https://discordapp.com/api/oauth2/authorize?client_id=388983602542804993&scope=bot&permissions=8)
Vous pouvez modifier les permissions, mais pensez bien qu'il doit avoir accès à tous les salons de jeu, doit pouvoir modifier les permissions des salons, les rôles des joueurses.

## Les rôles
Le bot a besoin de plusieurs rôles discord pour fonctionner.
le rôle « Morts », le rôles « Spectateurice » et le rôle « MJ ». Il doit avoir la possibilité de donner les 2 premiers rôles.

## Les salons
Il vous faut créer les salons que le bot reconnaîtra. Le nom de la Catégorie n'importe pas, mais le nom des salons est important.

![organisation des salons](https://powi.fr/LGBot-salons.png)

### Permissions des salons
Pour #village et #votes tout le monde peut y accéder.
Pour les autres salons, laissez juste la permission au rôle `MJ`, sauf pour le salon #morts, donnez la permission au rôle « Morts »
