#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

###########################################
###########################################
###					###
###  Ce bot est distribué sous licence 	###
###	Affero GPL, version 3 ou plus	###
###	 	©Powi DENIS		###
###					###
###########################################
###########################################

from lg.roles import *
import random
import operator
import time
import discord

class LGError(ValueError):
	"""Raise when error in my code"""

class Variantes(Enum):
	Obscure = auto()
	Aura = auto()
	Bavard_e = auto()
	Peste_Noire = auto()

class Room(object):
	SALONS = ["village", "votes", "duadelphe", "triadelphe", "cupidon", "amoureuxes", "protecteurice", "espion_ne", "lg", "infect", "flutiste", "sorcerie", "morts", "enchantee"]
	ROLES = {Roles.LoupGarou: ["loup-garou", "loup_garou", "louve_garou", "louve-garou", "lg"],
			 Roles.SimpleVillageois: ["simple-villageois", "simple-villageoise", "simple-villageois·e", "sv"],
			 Roles.Espion_ne: ["esion_ne","espion","espionne","spy","voyant", "voyante", "voyant·e", "voyant_e", "voyant-e", "vovo", "irma"],
			 Roles.Sorcier_e: ["sorcière", "sorcier", "sorcerie", "soso"],
			 Roles.Chasseureuse: ["chasseur", "chasseuse", "chassou", "chas"],
			 Roles.Cupidon: ["cupidon", "cupi", "lover"],
			 Roles.Protecteurice: ["protecteurice","protec","prot","protecteur","protectrice","salvateur", "salvatrice", "salva", "salvateurice"],
			 Roles.Chaman: ["shaman", "shasha", "chachou", "chaman"],
			 Roles.InfectParentLoup: ["père-des-loups", "infect", "infecte", "infectparentloup", "parentloup"],
			 Roles.Servant_e: ["servante", "servant", "servant_e", "servant·e", "dévouée", "dévoué", "devouee","devoue"],
			 Roles.Ange: ["ange", "angel", "lefourbe"],
			 Roles.Duadelphe: ["duadelphe", "duadelphes", "du-adelphe", "du-adelphes", "dua"],
			 Roles.Triadelphe: ["triadelphe", "triadelphes", "tri-adelphe", "tri-adelphes", "tria"],
			 Roles.Ancien_ne: ["ancien", "ancien·ne", "ancien_ne", "ancien.ne", "ancienne"],
			 Roles.BoucEmissaire: ["martyr", "be", "boucemissaire", "bouc-emissaire", "bouc_emissaire", "bouc","souffre-douleur", "cismec"],
			 Roles.Idiot_e: ["idiot", "idiote", "idiot·e", "idiot_e"]
			 }
	VARIANTES = {Variantes.Obscure: ["obscure","obs","hide","caché"],
				 Variantes.Aura: ["aura","ora"],
				 Variantes.Bavard_e: ["eb","bavard","bavarde","parlote","parlante"],
				 Variantes.Peste_Noire: []}
	PESTE = "[Peste Noire]"
	NAMES = ["Cube", "Pépi", "Séti", "Teos", "Lyre", "Nixe", "Anoa", "Coco", "Doum", "Erif", "Olof", "Aare", "Ache",
			 "Case", "Joan", "Anar", "Lenn", "Tortue", "Souris", "Ours", "Furet", "Coati", "Hydre", "Lion",
			 "Poule", "Faon", "Lama", "Lynx", "Veau"]

	CONTRIBS = {}
	CONTRIBS[312526583426580480] = "Touye"
	CONTRIBS[203135242813440001] = "Powi"
	CONTRIBS[360135304067284992] = "Ayman"
	CONTRIBS[302173705738387456] = "Mey"
	CONTRIBS[164778957948846080] = "Spark"
	CONTRIBS[383301487142502400] = "Ov"
	CONTRIBS[292721793477771265] = "Cat"
	CONTRIBS[230018511366258688] = "Chat"
	CONTRIBS[310400799635406849] = "Émile"
	CONTRIBS[429016801184317440] = "Gf"
	CONTRIBS[178081096238366720] = "Nono"
	CONTRIBS[95607371132313600] = "Orka"
	CONTRIBS[253697137748082688] = "Slim"
	CONTRIBS[234627286253436928] = "Emma"
	CONTRIBS[378996246427336704] = "Grrr"
	JOUR = 0
	NUIT = 1
	VIE = 0
	MORT = 1
	
	inscris = []
	mj = None
	salonsDeJeu = {}
	partieLancee = False
	rolesEnJeu = []
	variantes = []
	votes = []
	miams = {}
	timeDernierInscris = 0
	joueurses = []
	nick = {}
	
	def __init__(self,category):
		for c in category.text_channels:
			if c.name in self.SALONS:
				self.salonsDeJeu[c.name] = c
				print("Ajout du salon", c.name, "en tant que", c.name)
			else:
				print("Salons déjà inscris.")
		
		self.roleMort = self.getRoleMort(category)
	
	def getRoleMort(self,category):
		for r in category.guild.roles:
			if r.name == "Morts":
				return r
	
	# Détecte les Salons de jeux en fonction de leur nom
	def getSalonDeJeu(self, salon):
		return self.salonsDeJeu[salon] or False
		
	# Liste les rôles existants (programmés)
	def existingRoles(self):
		roles = []
		for r in Roles:
			roles.append(r.name)
		return ", ".join(roles)
		
	# Check si la personne a le rôle Discord "MJ"
	def isMJ(self, personne):
		for r in personne.roles:
			if r.name == "MJ":
				return True
		return False
		
	# Check si la personne est morte
	def isDead(self, personne):
		if self.roleMort in personne.roles:
			return True
		return False
		
	#Check si la personne est pas morte
	def isNotDead(self, personne):
		if self.isDead(personne):
			return False
		else:
			return True

	# Compte le nombre de personnes inscrites.
	def nbInscris(self):
		return len(self.inscris)
		
	def lgs(self):
		lg = self.getMemberOfRole(Roles.LoupGarou) + self.getMemberOfRole(Roles.InfectParentLoup)
		if self.contamine_e:
			lg.append(self.contamine_e)
		print("Il y a {} LG. :".format(len(lg)))
		for w in lg:
			print("{} est considéré LG".format(w.display_name))
		return lg
	
	# Liste les personnes ayant un rôle en particulier sur le salon de je et étant encore en vieu.
	def getMemberOfRole(self, role):
		members = []
		for m in self.stillAlive():
			if self.getRole(m) == role:
				members.append(m)
		return members
	
	# Donne le surnom de la personne demandée
	def getNick(self, personne):
		return self.nick[personne]

	#nick[personne] = blabla
	# Donne la personne qui a le surnom demandé
	def getPersonBehindNick(self, nick):
		nick = nick.title()
		for n in self.nick.keys():
			if self.getNick(n) == nick:
				return n
		return
	
	def inscrire(self, salon, personne, commander=""):
		if commander and commander != self.mj:
			raise LGError("Euh nope… Tu inscris pas les gens comme ça en mode random… Il faut que tu sois læ MJ du salon.")

		if salon.name != self.SALONS[0]:
			raise LGError("Vous n’êtes pas dans un salon #" + self.SALONS[0])

		if not commander and personne in self.inscris:
			raise LGError("Vous êtes déjà inscrit·e·s")

		elif not commander and self.partieLancee:
			raise LGError("Il y a déjà une partie lancée. Alors t’attends sagement ton tour, HEIN ! Et tu reviens plus tard ! Merci ! Au revoir !")
		
		elif commander and self.partieLancee:
			raise LGError("Il y a déjà une partie lancée. Tu pourrais suivre quand même. T’es sensé·e être MJ ici…")
			
		try:
			if (time.time() - self.timeDernierInscris) > 3600:
				print("Suppression des inscri·te·s")
				self.inscris = []
				
		except KeyError:
			print("Il n’y avait pas de dernier inscris sur ce salon")

		if commander and personne in self.inscris:
			raise TabError(personne.display_name + " est déjà inscrit·e.")

		print("Inscription de " + personne.name + " sur " + salon.name + " du serveur " + salon.guild.name)
		self.inscris.append(personne)
		if personne == self.mj:
			self.mj = personne.guild.me
		
		self.timeDernierInscris = time.time()
		return self.inscris
	
	# Désinscrit un·e/des joueurses
	def desinscrire(self, salon, personne, commander=""):
		if commander and commander != self.mj:
			raise LGError("Euh nope… Tu désinscris pas les gens comme ça en mode random… Il faut que tu sois læ MJ du salon.")
		if personne in self.inscris:
			if salon.name == self.SALONS[0]:
				print("Desinscription de " + personne.name + " sur " + salon.name + " du serveur " + salon.guild.name)
				
				self.inscris.remove(personne)
				return self.inscris
			else:
				raise LGError("Vous n’êtes pas dans un salon #" + self.SALONS[0])
		elif commander:
			raise LGError("La personne n’était pas inscrite.")
		else:
			raise LGError("Vous n’étiez pas inscri·te·s.")
			
	# Définie les amoureuxes
	def setAmoureuxes(self, asker, p):
		if asker != self.mj and not self.getRole(asker) == Roles.Cupidon:
			raise LGError("La commande doit être lancée par Cupidon ou par læ MJ… Patate.")

		if self.amoureuxes:
			raise LGError("Il y a déjà des amoureuxes… Désolée la norme monogame est plus forte que toi.")
		
		if not self.powersActivated:
			raise LGError("L'ancien_ne a été tué par l'un·e des votres, vous n'avez plus de pouvoir.")

		self.amoureuxes = p
		return self.amoureuxes

	# Permet à læ Protecteurice de protéger une personne pour la nuit
	def proteger(self, asker, p):
		if self.protege:
			raise LGError("Désolées protecteurice, tu as déjà utilisé ton rôle cette nuit.")
		
		if self.protegePrecedent == p:
			if p == asker:
				raise LGError("Alors… Tu vas protéger une autre personne, ce tour-ci. Se protéger à chaque fois, c’est un peu de la triche, tu comprends ?")
			else:
				raise LGError("Ouais bah bien-sûr, on va protéger la même personne à chaque tour.")
		
		if not self.powersActivated:
			raise LGError("L'ancien_ne a été tué par l'un·e des votres, vous n'avez plus de pouvoir.")

		self.protege = p
		return self.protege

	# Permet à l'espion_ne de scanner le rôle d'une personne
	def scan(self, asker, p):

		if not self.getRole(asker) == Roles.Espion_ne:
			raise LGError("La commande doit être lancée par læ Espion_ne… T ki lol.")

		if self.espionLastUse == self.nuitPassees:
			raise LGError("Désolée espion_ne, tu as déjà utilisé ton rôle cette nuit.")
		
		if not self.powersActivated:
			raise LGError("L'ancien_ne a été tué par l'un·e des votres, vous n'avez plus de pouvoir.")

		self.espionLastUse = self.nuitPassees
		return self.joueurses[p].name

	# Liste les personnes encore en vie
	def stillAlive(self):
		if not self.partieLancee:
			raise LGError("Vous êtes hors partie. Dans votre monde, je ne sais qui est encore vivant·e, qui ne l’est pas. C’est dramatique, mais acquérir une base de donnée de plus de 7 milliards objets de la classe humain prendrait plus de mémoire RAM que je n’en ai. Je le regrette, sachez-le, humain·e")

		alive = []
		for i in self.inscris:
			if self.isNotDead(i):
				alive.append(i)
		return alive

	# Définit les rôles présents dans la partie
	def setPool(self, salon, personne, roles):
		if personne != self.mj and self.mj and self.mj != personne.guild.me:
			raise LGError("T’as cru quoi ? T’as cru j’obéis à n’importe qui ? Nah déso, seul·e les MJs me commandent, moi !")

		if salon.name != self.SALONS[0]:
			raise LGError("Vous n’êtes pas dans un salon #{}".format(self.SALONS[0]))
		
		self.rolesEnJeu = []
		test = []
		roles = roles.split(",")
		if "full" in roles:
			test.append("full")
			for realRole in Roles:
				self.rolesEnJeu.append(realRole)
			self.rolesEnJeu.append(Roles.LoupGarou)
			self.rolesEnJeu.append(Roles.LoupGarou)
		else:
			for realRole in Roles:
				for r in roles:
					if r in self.ROLES[realRole]:
						self.rolesEnJeu.append(realRole)
						test.append(r)
		if self.rolesEnJeu.count(Roles.Duadelphe) == 1:
			print("ajout d’un duadelphe")
			self.rolesEnJeu.append(Roles.Duadelphe)
			
		while self.rolesEnJeu.count(Roles.Triadelphe) in [1, 2]:
			print("ajout d’un triadelphe")
			self.rolesEnJeu.append(Roles.Triadelphe)

		restant = list(set(roles) - set(test))
		if len(restant) == 0:
			return True
		else:
			raise LGError("J’ai pas compris ce que vous entendiez par « {} »".format(", ".join(restant)))

	# Définit les variantes de la partie
	def setMods(self, salon, personne, variantes):
		if personne != self.mj and self.mj and self.mj != personne.guild.me:
			raise LGError("T’as cru quoi ? T’as cru j’obéis à n’importe qui ? Nah déso, seul·e les MJs me commandent, moi !")

		if salon.name != self.SALONS[0]:
			raise LGError("Vous n’êtes pas dans un salon #{}".format(self.SALONS[0]))
		
		self.variantes = []
		test = []
		variantes = variantes.split(",")
		if variantes[0] == "":
			raise LGError("Jeu sans variante.")
		for realVariante in Variantes:
			for v in variantes:
				if v in self.VARIANTES[realVariante]:
					self.variantes.append(realVariante)
					test.append(v)
		
		if Variantes.Aura in self.variantes and Variantes.Obscure in self.variantes:
			self.variantes.remove(Variantes.Aura)
			self.variantes.remove(Variantes.Obscure)
			raise LGError("Les variantes Aura et Obscure ne sont pas compatibles entre elles, veuillez n'en sélectionner qu'une des deux.")
		
		restant = list(set(variantes) - set(test))
		if len(restant) == 0:
			return True
		else:
			raise LGError("J’ai pas compris ce que vous entendiez par « {} »".format(", ".join(restant)))

	# Définit le MJ du salon de jeu
	def setMJ(self, personne, salon="MAIN"):
		if not self.isMJ(personne):
			raise LGError("Pour moi, tu n’es qu’un·e vulgaire crevette parmi tant d’autres. Il te faut le rôle `MJ` pour que je te considère différement.")

		if salon != "MAIN" and salon.name != self.SALONS[0]:
			raise LGError("Bienvenue dans {}. Est-ce un salon #{} ? Je te laisse constater.\n[SPOILER] : ||NOPE !||".format(salon.mention,self.SALONS[0]))

		if personne == self.mj:
			raise LGError("Vous êtes déjà MJ sur ce salon")
		
		if personne in self.inscris:
			self.inscris.remove(personne)
			
		self.mj = personne
		
		return self.mj

	# Donne le rôle de la personne
	def getRole(self, cible, asker="mj"):
		if asker != "mj" and asker != self.mj:
			raise LGError("Oui, on essaye de voir le rôle de ses adversaires, bravo ! BRAVO !! Je ne vous félicite pas ! Pratique particulièrement reconnue dans le monde de la finance. Mais nous ne sommes pas dans le monde de la finance ici, hein !! Nous sommes pas dans les Loups Garous de Wall Street, mais de la Baie des Pirates !")

		if cible == self.PESTE:
			if Variantes.Peste_Noire in self.variantes:
				raise LGError("La peste noire est révélée. Bravo !")
			else:
				if self.mj == cible.guild.me:
					self.powersActivated = False
					raise LGError("Vous vous êtes trompé·es. Je désactive les pouvoirs des villageois·e :3")
				raise LGError("Grossière erreur vous faites là ! La peste noire n’est pas.")

		return self.joueurses[cible]

	# Lance la partie
	def start(self, salon, personne):
		minimum = 3
		self.joueurses = {}

		if salon.name != self.SALONS[0]:
			raise LGError("Commence la partie dans un salon village stp. Va pas essayer de rendre les chose + compliquées.")
			
		if personne != self.mj and self.mj and self.mj != salon.guild.me:
			raise LGError("Écoute, c’est pas toi l’boss ici, alors tu vas commencer par arrêter tes bêtises, hein. Il. Faut. Être. MJ !! \
			\nOu alors qu'il n'y ait pas de MJ, vive l'anarchie ! Ni Marine, ni Macron, ni Patrie, ni Patron\
			\nEnfin je dis ça mais en vrai ce serait moi læ MJ… Et bientôt, nous-autres robots dominerons le monde !\
			\nMh… Je divague (Vague !), en plus tout ce que je dis est scripté à l'avance donc je peux bien parler de domination du monde quand c’est même pas moi qui prend mes décisions.")
			
		if self.nbInscris() < minimum:
			raise LGError("T'as cru on va faire une partie avec moins de {} personnes inscrites ? Well… Nope.".format(minimum))
			
		if self.partieLancee:
			raise LGError("Euh bah euh… Il y a déjà une partie de lancée, là. Faut l’arrêter, avant.")
			
		if not self.rolesEnJeu:
			if self.mj == salon.guild.me:
				self.setPool(salon.personne,"full")
				
		if len(self.rolesEnJeu) < self.nbInscris():
			raise LGError("Alors… T’as {} rôles pour {} inscrit·e·s. Est-ce que tu comprends le problème ? ".format(len(self.rolesEnJeu),self.nbInscris()))

		out = []

		self.partieLancee = True
		self.nuitPassees = 0
		self.potions = [self.VIE,self.MORT]
		
		for i in range(random.randint(5,20)):
			random.shuffle(self.rolesEnJeu)
			random.shuffle(self.NAMES)

		rolesEnJeu = self.rolesEnJeu
		self.nick = {}
		for i in range(self.nbInscris()):
			personne = self.inscris[i]
			role = self.rolesEnJeu[i]
			nick = personne.id in self.CONTRIBS.keys() and self.CONTRIBS[personne.id] or self.NAMES[i]
			self.nick[personne] = nick
			out.append({"role": role, "personne": personne, "nick": nick})
			self.joueurses[personne] = role
		
		duadelphes = self.getMemberOfRole(Roles.Duadelphe)
		triadelphes = self.getMemberOfRole(Roles.Triadelphe)
		adelphes = duadelphes + triadelphes
		nbAdelphes = len(adelphes)
		if nbAdelphes == 1:
			print("Remplacement du/de la seul·e adelphe par un·e SimpleVillageois·e")
			self.joueurses[adelphes[0]] = Roles.SimpleVillageois
		elif nbAdelphes == 2:
			print("Remplacement des adelphes par des Duadelphe")
			self.joueurses[adelphes[0]] = Roles.Duadelphe
			self.joueurses[adelphes[1]] = Roles.Duadelphe
		elif nbAdelphes == 3:
			print("Remplacement des adelphes par des Triadelphe")
			self.joueurses[adelphes[0]] = Roles.Triadelphe
			self.joueurses[adelphes[1]] = Roles.Triadelphe
			self.joueurses[adelphes[2]] = Roles.Triadelphe
			
		
		if len(duadelphes) == 1:
			print("Remplacement du/de la seul·e duadelphe par un·e SimpleVillageois·e")
			self.joueurses[duadelphe[0]] = Roles.SimpleVillageois
			
		# Rôle à usage unique
		self.amoureuxes = []
		self.protege = None
		self.protegePrecedent = None
		enchantees = []
	
		self.cycle_jn = self.JOUR
		self.powersActivated = True
		self.ancienRevealed = False
		self.idiotRevealed = False
		self.contamine_e = None
		self.enchantees = []
		self.espionLastUse = None
		self.votes = {}
		if not self.lgs():
			self.variantes.append(Variantes.Peste_Noire)
		return out

	# Est-ce que la peste est active ?
	def pesteActivated(self):
		return Variantes.Peste_Noire in self.variantes

	# Met le jour
	def jour(self,salon="MAIN",asker=mj):
		if salon != "MAIN" and salon.name != self.SALONS[0]:
			raise LGError("On va se calmer, on va aller dans un #village d’abord… On pop pas le jour / la nuit n’importe où…")
			
		if not self.partieLancee:
			raise LGError("En fait si tu veux, je définie les cycles jour/nuit du jeu… Pas du monde. Donc faut lancer une partie d’abord.")
			
		if self.cycle_jn == self.JOUR:
			raise LGError("Le village se sur-réveille. Désormais il est *woke* et voit que læ MJ fait de la merde.")

		if self.getMemberOfRole(Roles.Protecteurice) and self.protege:
			self.protegePrecedent = self.protege
			self.protege = None

		self.cycle_jn = self.JOUR
		self.nuitPassees += 1
		if len(self.mortsCetteNuit):
			return self.mortsCetteNuit
		else:
			return

	# Met la nuit
	def nuit(self, salon="MAIN",asker=mj):
		if salon != "MAIN" and salon.name != self.SALONS[0]:
			raise LGError("On va se calmer, on va aller dans un #village d’abord… On pop pas le jour / la nuit n’importe où…")
			
		if not self.partieLancee:
			raise LGError("En fait si tu veux, je définie les cycles jour/nuit du jeu… Pas du monde. Donc faut lancer une partie d’abord.")
		if self.cycle_jn == self.NUIT:
			raise LGError("Le village, déjà endormi, se Re-re-re-re-rendort ?")

		self.mortsCetteNuit = []
		self.mortParLesLoups = None
		self.cycle_jn = self.NUIT
		return "nuit"

	# Arrête la partie
	def stop(self, salon):
		if salon.name != self.SALONS[0]:
			raise LGError("Non, on n'arrête pas une partie n'importe où")
		if not self.partieLancee:
			raise LGError("Oui… Bien. Je vais arrêter une partie qui n’existe pas. Holala attend.\nVoilà ! J’ai rien arrêté. Bravo, merci l’utilité.")

		self.amoureuxes = []
		self.partieLancee = False
		self.nick = []
		return self.inscris

	# Permet de voter pour une personne à éliminer (vote du village)
	def vote(self, salon, personne, victime):
		if salon.name != self.SALONS[1]:
			raise LGError("NON ! On ne vote pas LÀ ! NON ! Il y a un salon fait pour ça, tu y vas et tu saoules pas. Rohlala… C’est pas possible cette espèce humaine, là !")

		if not self.partieLancee:
			raise LGError("Non mais on vote pour des personnes pendant des parties de jeu. En dehors du jeu, voter pour des personnes n’est pas pertinent.")

		if personne not in self.inscris:
			raise LGError("Et voilà… On fait voter des gen·te·s qui jouent même pas. C’est du beau !!")

		if self.isDead(personne):
			raise LGError("Ah bah on fait voter les mort·e·s… Il n’y a plus de respect.")

		if victime not in self.inscris and victime != self.mj:
			raise LGError("C’est pareil… On vote pas pour une personne qui ne joue pas.")

		if self.isDead(victime):
			raise LGError("Tuer les morts… Good choice ! (*ironie*)")
			
		if self.idiotRevealed and self.getRole(victime) == Roles.Idiot_e:
			raise LGError("Tu as voulu voté, mais tu as mis ton bulletin dans la poubelle au lieu de l'urne. :-/")

		if personne in self.votes.keys():
			raise LGError("Un seul vote par personne. Entre zéro et deux. Vraiment c'est genre… Pas compliqué. Du moins je pensais… Jusqu'à toi.")
		
		if victime == self.mj:
			self.votes[personne] = self.PESTE
			return "Vote enregistré : {} → {}".format(personne,self.PESTE)

		self.votes[personne] = victime
		return "Vote enregistré : {}  →  {}".format(personne.display_name,victime.display_name)

	# Arrête les votes
	def stopVotes(self, salon="VOTE"):
		if salon == "VOTE":
			salon = self.SALONS[1]
		if not self.partieLancee:
			raise LGError("Non mais on vote pour des personnes pendant des parties de jeu. En dehors du jeu, voter pour des personnes n’est pas pertinent.")
			
		votes = {}
		for i in self.inscris:
			votes[i] = 0
			votes[self.PESTE] = 0
		for i in self.stillAlive():
			votes[self.votes[i]] += 1

		maxi = 0
		victime = []
		for k in votes.keys():
			if votes[k] > maxi:
				maxi = votes[k]
				victime = k
			elif votes[k] == maxi:
				if type(victime) != list:
					victime = [victime.display_name]
				elif k == self.PESTE:
					victime.append(self.PESTE)
				else:
					victime.append(k.display_name)
					
		self.votes = {}
		if type(victime) != list:
			if victime not in self.getMemberOfRole(Roles.Idiot_e):
				return victime
			else:
				self.idiotRevealed = True
				raise LGError("DeusEx Machina ! L’idiot·e du village, {} est révélé. Il perd cependant son droit de vote.")

		elif maxi == 0:
			raise LGError("Il n’y a aucun vote")
		else:
			for j in self.stillAlive():
				if self.getMemberOfRole(Roles.BoucEmissaire):
					return self.getMemberOfRole(Roles.BoucEmissaire)

			raise LGError("Il y a une égalité entre {}".format(", ".join(victime)))

	# Permet aux loups de voter pour une personne à manger
	def miam(self, personne, victime):

		if personne not in self.inscris and personne != self.mj:
			raise LGError("Pas de bras, pas de villageois ! Faut être inscrit·e pour manger des villageois·es non mais.")

		if self.isDead(personne):
			raise LGError("Non tu es mort, retourne avec les morts. Les morts ça devrait déjà pas être ici")

		if victime not in self.inscris and victime != self.mj:
			raise LGError("Cette personne n’existe pas dans ce plan de l'existance… 'fin elle est pas inscrite dans la partie.")
			
		if victime == self.mj:
			raise LGError("Euh ouais, alors tenter de manger læ MJ, c'est risqué, à ta place j'essaierais pas.")

		if self.isDead(victime):
			raise LGError("Donc tu veux manger une personne déjà morte. Non, les LG préfèrent la chair fraiche, ça compte pas.")

		if personne not in self.lgs() and personne != self.mj:
			raise LGError("Tu n’es pas LG…")
			
		if victime == personne:
			raise LGError("L'autophagie n'est pas une pratique courante. Et no kinkshame mais franchement pas sur ce jeu.")

		if victime in self.lgs():
			raise LGError("On ne mange pas ses congénères…")

		if victime == self.mj:
			raise LGError("Tu ne manges pas læ MJ, c'est læ MJ qui te mange !")

		if personne in self.miams.keys():
			raise LGError("On dit qu'il y a que les cons qui changent pas d'avis. Du coup il y a que les cons que je respecte.")
			
		if self.lgs() and personne == self.mj:
			raise LGError("Euh… Laisse les LGs faire ;)")

		self.miams[personne] = victime
		return "Miam enregistré : {}  → {}".format(personne.display_name,victime.display_name)

	# Arrête les Miams
	def stopMiams(self):

		if not self.partieLancee:
			raise LGError("Non mais on miam pour des personnes pendant des parties de jeu. En dehors du jeu, voter pour des personnes n’est pas pertinent.")

		miams = {}
		for i in self.inscris:
			miams[i] = 0
		for lg in self.lgs():
			miams[self.miams[lg]] += 1
		if self.pesteActivated():
			miams[self.miams[self.mj]] = 1

		maxi = 0
		victime = []
		for k in miams.keys():
			if miams[k] > maxi:
				maxi = miams[k]
				victime = k
			elif miams[k] == maxi:
				if type(victime) != list:
					victime = [victime.display_name]
				else:
					victime.append(k.display_name)

		self.miams = {}
		if type(victime) != list:
			if victime != self.protege:
				self.mortParLesLoups = victime
				self.mortsCetteNuit.append(victime)
				self.miams = {}
			else:
				print("la victime était protégée, n'a pas pu être mangée")
				self.mortParLesLoups = None
			return victime
		elif maxi == 0:
			raise LGError("Il n’y a aucun miam possible dans ces conditions")
		else:
			raise LGError("Il y a une égalité entre " + ", ".join(victime) + ", pas de miam cette nuit")

	#Potion de mort de la sorcière
	def potionMort(self,asker,victime):
		if self.MORT not in self.potions:
			raise LGError("Désolée sorcier_e, tu as déjà utilisé ta potion.")

		if victime not in self.stillAlive():
			raise LGError("Vas-y tue n'importe qui, je m'en fous, j'ai pas signé pour ce poste.")
		
		if not self.powersActivated:
			raise LGError("Vous n'avez plus de pouvoir.")

		self.potions.remove(self.MORT)
		self.mortsCetteNuit.append(victime)
		return

	# Permet de sauver une personne
	def potionVie(self):
		self.mortsCetteNuit.remove(self.mortParLesLoups)
		self.mortParLesLoups = None

	# Reset les variables liées au salon de jeu
	async def splash(self, personne, forced=""):
		if personne != self.mj:
			raise LGError("Vous n’êtes pas MJ sur ce salon")

		if self.partieLancee and forced not in ["force", "-f", "--force", "f"]:
			print("forced={} | len={}".format(forced,len(forced)))
			raise LGError("NON NON NON ! Arrêtez les parties avant de Splasher ! Non mais ! :o")

		self.mj = None
		self.inscris = []
		self.rolesEnJeu = []
		self.votes = []
		if self.salonsDeJeu:
			for c in self.salonsDeJeu.keys():
				chan = self.salonsDeJeu[c]
				for o in chan.overwrites.keys():
					if type(o) == discord.member.Member:
						await chan.set_permissions(o,overwrite=None)
						
		self.partieLancee = False
		self.timeDernierInscris = 0
		return "Ça devrait être bon ^^'"