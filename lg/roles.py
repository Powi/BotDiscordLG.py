#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

###########################################
###########################################
###					###
###  Ce bot est distribué sous licence 	###
###	Affero GPL, version 3 ou plus	###
###	 	©Powi DENIS		###
###					###
###########################################
###########################################

from enum import Enum, auto

class Roles(Enum):
	LoupGarou = auto()
	SimpleVillageois = auto()
	Espion_ne = auto()
	Sorcier_e = auto()
	Chasseureuse = auto()
	Cupidon = auto()
	Protecteurice = auto()
	Chaman = auto()
	InfectParentLoup = auto()
	Servant_e = auto()
	Ange = auto()
	Duadelphe = auto()
	Triadelphe = auto()
	Ancien_ne = auto()
	BoucEmissaire = auto()
	Idiot_e = auto()
	
class Camp(Enum):
	LoupGarou = auto()
	Villageois_e = auto()
	Solitaire = auto()

class AbstactRole(object):
	__description = ""
	salons = []
	
	def setSalon(salon):
		self.salon = salons

	def __init__(self,salons):
		self.salons = salons
		return
	
class CampLoupGarou(AbstactRole):
	camp = Camp.LoupGarou
	roles = [Roles.LoupGarou,Roles.InfectParentLoup]
	__descriptionCamp = "Camp = LG : Leur but est d’éliminer toutes les personnes qui ne sont pas dans leur camp."
	
	def __init__(self):
		return
	
	def descriptionCamp(self):
		return self.__description
 
class CampVillageois_e(AbstactRole):
	camp = Camp.Villageois_e
	roles = [Roles.SimpleVillageois,Roles.Espion_ne,Roles.Sorcier_e,Roles.Chasseureuse,Roles.Cupidon,Roles.Protecteurice,
			 Roles.Chaman,Roles.Servant_e,Roles.Duadelphe,Roles.Triadelphe,Roles.Ancien_ne,Roles.BoucEmissaire,Roles.Idiot_e]
	__descriptionCamp = "Camp = Villageois_e : Leur but est d'éliminer toutes les personnes dans le camp des LG et les éventuels rôles solitaires."

class CampsSolitaire(AbstactRole):
	camp = Camp.Solitaire
	roles = []

class LoupGarou(CampLoupGarou):
	__description = ""
	#salon_name = 
	def __init__(self):
		self.setSalons = {"lg":"rw"}
		self.__description = "Ce sont les simples LG. "+self.__descriptionCamp
		return


class SimpleVillageois(CampVillageois_e):
	def __init__(self):
		self.__description = "Iels n'ont pas de rôle particulier."+self.__descriptionCamp
		
class Espion_ne(CampVillageois_e):
	def __init__(self):
		self.__description = "Iel peut connaître le rôle d'une personne chaque nuit" + self.__descriptionCamp
