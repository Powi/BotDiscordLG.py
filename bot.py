#!/usr/bin/python3.7
# -*- coding: utf-8 -*-

# Command line parameter:
# 1: Discord TOKEN

###########################################
###########################################
###					###
###  Ce bot est distribué sous licence 	###
###	Affero GPL, version 3 ou plus	###
###	 	©Powi DENIS		###
###		©Gronse		###
###					###
###########################################
###########################################

import discord
import sys
import json
import traceback
import io
import re
import math
from datetime import *
import time
import asyncio
import random
import dateutil.parser

from PIL import Image, ImageDraw, ImageFont
import requests
import os

from lg.room import *
from lg.roles import *
from utils.const import Const

client = discord.Client()

### Fonction codée par Gronse et adaptée par Powi
async def GUI_EllipseJeu(bgColor,font,tailleCercle,channel):
	print("[+] Creating GUI")
	PI = 3.14159
	category = getRoom(channel)
	nbJoueur = category.nbInscris()
	MJ = category.mj
	Tdefault = tailleCercle*5
	Tnormal = (tailleCercle*nbJoueur/PI)+tailleCercle
	T = int(max(Tdefault,Tnormal))
	rTx = 16/9
	cercleDiametre = T-tailleCercle

	imgCanvas = Image.new('RGBA',(int(T*rTx),T),bgColor)
	imgCanvasDraw = ImageDraw.Draw(imgCanvas)

	taille = int(tailleCercle*19/23)
	rCercleP = (taille/2)*(23/20) #rayon du grand cercle de couleur
	rCercleT = (taille/2)*(11/10) #rayon du cercle de "transparence"
	decalageY = tailleCercle/2-rCercleP #decalage de cercle verticalement
	fnt =  ImageFont.truetype(font,int(taille/4))
	fntSmall = ImageFont.truetype(font,int(taille/7))

	#Masque en Cercle pour le calque alpha
	circle = Image.new('L',(taille,taille),0)
	drawCircle = ImageDraw.Draw(circle)
	drawCircle.ellipse((0, 0, taille, taille), fill=(255), outline=(255))

	#MJ
	mjColor = (255,200,0,255)
	#si photo existe ou non
	raw_img_MJ = requests.get(MJ.avatar_url_as(format="png",size=128), stream=True).raw
	imgProfile = Image.open(raw_img_MJ)
	imgProfile = imgProfile.resize((taille,taille))
	if MJ == channel.guild.me:
		pseudoMJ = "Louve Garou\nBTI+ [Bot]"
	else:
		pseudoMJ = MJ.display_name
	pxlMJ = imgCanvasDraw.textsize("| MJ |",fnt)
	pxlPseudoMJ = imgCanvasDraw.textsize(pseudoMJ,fnt)
	imgCanvasDraw.rectangle((int(T*rTx/2-pxlMJ[0]*3/5),int(T/2),int(T*rTx/2+pxlMJ[0]*3/5),int((T/2-taille*12/20-pxlMJ[1]*3/2)-decalageY)), fill=mjColor, outline=mjColor)
	imgCanvasDraw.text((int(T*rTx/2-pxlMJ[0]/2),int((T/2-taille*12/20-pxlMJ[1])-decalageY)), "| MJ |", fill=bgColor, font=fnt)
	imgCanvasDraw.multiline_text((int(T*rTx/2-pxlPseudoMJ[0]/2),int((T/2+taille*12/20)-decalageY)), pseudoMJ, fill=mjColor, font=fnt, spacing=1, align="center")
	imgCanvasDraw.ellipse((int(T*rTx/2-rCercleP),int((T/2-rCercleP)-decalageY),int(T*rTx/2+rCercleP),int((T/2+rCercleP)-decalageY)), fill=mjColor, outline=mjColor)
	imgCanvasDraw.ellipse((int(T*rTx/2-rCercleT),int((T/2-rCercleT)-decalageY),int(T*rTx/2+rCercleT),int((T/2+rCercleT)-decalageY)), fill=bgColor, outline=bgColor)
	imgCanvas.paste(imgProfile,(int(T*rTx/2-taille/2),int((T/2-taille/2)-decalageY)),circle)
	print("Mj dessiné.e !")

	for i in range(category.nbInscris()):
		profil = category.inscris[i]
		#Calcule Centre de l'image
		angleJoueur = 2*PI*i/nbJoueur
		centre =(((math.cos(angleJoueur)*cercleDiametre/2)+(T/2))*rTx,(math.sin(angleJoueur)*cercleDiametre/2)+(T/2)-decalageY)
		#Profil Joueur
		playerColor = (46,204,113,255)
		#import image depuis url
		try:
			raw_img = requests.get(profil.avatar_url_as(format="png",size=128), stream=True).raw
			imgProfil = Image.open(raw_img)
			imgProfil = imgProfil.resize((taille,taille))
		except:
			imgProfil = Image.new('L',(taille,taille),0)
			print("/!\ image de {} non trouvée".format(profil.display_name))
		#test si mort.e
		if category.isDead(profil): #mort.e
			playerColor = (100,100,100,255)
			imgProfil = imgProfil.convert('L')
		#collage photo
		imgCanvasDraw.ellipse((int(centre[0]-rCercleP),int(centre[1]-rCercleP),int(centre[0]+rCercleP),int(centre[1]+rCercleP)), fill=playerColor, outline=playerColor)
		imgCanvasDraw.ellipse((int(centre[0]-rCercleT),int(centre[1]-rCercleT),int(centre[0]+rCercleT),int(centre[1]+rCercleT)), fill=bgColor, outline=bgColor)
		imgCanvas.paste(imgProfil,(int(centre[0]-taille/2),int(centre[1]-taille/2)),circle)

		centreText = (centre[0]+(math.cos(angleJoueur+PI)*taille*13/20),centre[1]+(math.sin(angleJoueur+PI)*taille*13/20))
		pseudo = profil.display_name
		if category.getNick(profil)=="":
			pxlPseudo = imgCanvasDraw.textsize(pseudo,fnt)
			while pxlPseudo[0]>tailleCercle:
				pseudo = pseudo[:-4]+"..."
				pxlPseudo = imgCanvasDraw.textsize(pseudo,fnt)
			posTextX = int(centreText[0]-(((math.cos(angleJoueur)/2)+0.5)*pxlPseudo[0]))
			posTextY = int(centreText[1]-(((math.sin(angleJoueur)/2)+0.5)*pxlPseudo[1]))
			imgCanvasDraw.text((posTextX,posTextY), pseudo, font=fnt, fill=playerColor)
		else:
			surnom = category.getNick(profil)
			pxlSurnom = imgCanvasDraw.textsize(surnom,fnt)
			while pxlSurnom[0]>taille*rTx:
				surnom = surnom[:-4]+"..."
				pxlSurnom = imgCanvasDraw.textsize(surnom,fnt)
			posTextX = int(centreText[0]-(((math.cos(angleJoueur)/2)+0.5)*pxlSurnom[0]))
			posTextY = int(centreText[1]-(((math.sin(angleJoueur)/2)+0.5)*pxlSurnom[1]))
			imgCanvasDraw.text((posTextX,posTextY), surnom, font=fnt, fill=playerColor)
			pxlPseudo = imgCanvasDraw.textsize("[ @{} ]".format(pseudo),fntSmall)
			while pxlPseudo[0]>taille*rTx:
				pseudo = pseudo[:-4]+"..."
				pxlPseudo = imgCanvasDraw.textsize("[ @{} ]".format(pseudo),fntSmall)
			posTextX = int(centreText[0]-(((math.cos(angleJoueur)/2)+0.5)*pxlPseudo[0]))
			if angleJoueur > 0  and angleJoueur < PI:
				imgCanvasDraw.text((posTextX,posTextY-pxlSurnom[1]), "[ @{} ]".format(pseudo), font=fntSmall, fill=(150,150,150,255))
			else:
				imgCanvasDraw.text((posTextX,posTextY+pxlSurnom[1]), "[ @{} ]".format(pseudo), font=fntSmall, fill=(150,150,150,255))
		print("profil de {} dessiné.e !".format(pseudo))

	j = 0
	while os.path.isfile("EllipseParticipantEs_{}.png".format(j)) :
		j = j+1
	
	filename = "EllipseParticipantEs_{}.png".format(j)
	imgCanvas.save(filename)
	await channel.send(file=discord.File(filename))
	os.remove(filename)

def errlog(message):
	guild = message.guild
	for c in guild.channels:
		if c.name == "errlog":
			return c
	return message.channel

def seek(line):
	items = ["built","seen","sleep","build","sang","bit","smelt","make","mow","sawn","sowed","known","will","caught","broadcast","get","fight","took","thrown","hold","rung","said","swept","rode","see","hurt","may","drawn","shrunk","stink","fought","spoken","learnt","had","chose","dig","weep","forgotten","arise","stuck","bled","ride","sing","became","forgave","grew","bent","sweep","bound","mean","lean","catch","burn","light","stick","bite","smell","dream","spoke","show","run","meant","wear","can","was","won","swing","knelt","cut","feed","know","deal","arose","swim","told","give","become","ring","hang","shaken","wore","shall","break","read","grown","'ve","have","were","began","sown","showed","spent","burst","shut","chosen","stole","got","lose","gone","woken","bind","lead","struck","say","driven","take","rang","sold","arisen","shake","wrote","lay","tore","find","tear","put","write","awake","hidden","lain","burned","swam","win","buy","sunk","hear","came","spend","drove","shine","froze","shot","shrank","learn","meet","brought","slide","lighted","sewn","shrink","sank","speak","sow","shown","spill","spread","written","eat","overtook","bore","bear","taken","ate","dreamed","thought","held","drank","bring","saw","shoot","kept","could","sent","tell","found","bred","met","must","crept","spat","threw","creep","hid","feel","broken","did","cost","swollen","spilt","keep","should","wind","flown","paid","begin","sewed","leaned","choose","overtake","forbade","led","swell","lost","stunk","forbidden","draw","frozen","leant","would","forgive","wound","fly","cling","forgot","sat","bitten","lit","bend","drew","understand","done","dug","grind","swum","steal","broke","awoken","bet","fall","spelt","lie","throw","dealt","breed","gave","beat","blew","dreamt","blow","forgiven","leave","stood","rose","hit","sawed","send","sung","spit","ground","heard","bought","shone","rise","made","lied","pay","spell","might","knew","beaten","hadto","mown","swore","ridden","stank","spilled","torn","drunk","woke","stung","risen","shed","lent","fed","been","awoke","wept","begun","stand","born","borne","freeze","hung","sell","learned","slept","overtaken","given","drink","swear","come","burnt","taught","bleed","sink","worn","clung","forget","drive","think","felt","fell","spelled","eaten","fallen","kneel","swung","laid","went","stolen","sit","ran","sew","sting","wake","blown","left","be","teach","beenable","go","hide","grow","forbid","set","slid","flew","shook","swelled","strike","mowed","understood","sworn"]
	
	line = re.sub("[.,:?!<>/]"," ",line)
	line = line.lower()
	line = line.split()
	found = []
	for l in line:
		if l in items:
			found.append(l)
		elif l.endswith("ed"):
			found.append("{}[-ed]".format(l))
		elif l.endswith("en") and l not in ["when","darren","kitchen","then","garden","childen"]:
			found.append("{}[-en]".format(l))
	return found

def emoji(guild, name):
	for e in guild.emojis:
		if e.name == name:
			return e
	return ""

def getM(message,msg):
	mention = re.search('<@!?([0-9]*)>', msg)
	category = getRoom(message.channel)
	if mention == None:
		m = category.getPersonBehindNick(msg)
	else:
		m = message.guild.get_member(int(mention.group(1)))
	
	if not category.partieLancee:
		raise LGError("Mmh. Je propose un truc : On joue pas quand aucune partie n'est démarrée. Je sais ça a l'air révolutionnaire.")
	return m

def hasRole(personne, role):
	for r in personne.roles:
		if r.name == role:
			return True
	return False

def getName(g,k):
	if(g.get_member(k)):
		return g.get_member(k).display_name
	elif(client.get_user(k)):
		return client.get_user(k).name
	elif k == -1:
		return "Anonyme"
	elif k == 95607371132313600:
		return "Orka"
	else:
		print (client.get_user(k))
		return "Compte discord supprimé"

def isModo(personne):
	for r in personne.roles:
		if r.name in ["Modo", "Admin"]:
			return True
	return False

def channel(guild, name):
	for c in guild.text_channels:
		if c.name in name:
			return c

def keywords(message):
	msgContent = message.content.strip()
	msgKeywords = msgContent.split(" ")
	return msgKeywords
	
def isInRoom(category):
	for c in category.channels:
		if c.name.lower() in ["village","baie"]:
			return True
	return False

def getRoom(channel):	
	category = channel.category
	if isInRoom(category):
		if category not in rooms.keys():
			rooms[category] = Room(category)
		return rooms[category]
	raise LGError("Vous n'êtes pas dans un salon jouable")

async def changePerms(j, chan):
	category = getRoom(chan)
	salons = {}
	salons[Roles.Cupidon] = [{"self": "cupidon", "perm": "rw"}]
	salons[Roles.LoupGarou] = [{"self": "lg", "perm": "rw"}]
	salons[Roles.Espion_ne] = [{"self": "espion_ne", "perm": "rw"}]
	salons[Roles.Sorcier_e] = [{"self": "sorcerie", "perm": "rw"}]
	salons[Roles.Protecteurice] = [{"self": "protecteurice", "perm": "rw"}]
	salons[Roles.Chaman] = [{"self": "morts", "perm": "r"}]
	salons[Roles.InfectParentLoup] = [{"self": "infect", "perm": "rw"}, {"self": "lg", "perm": "rw"}]
	salons[Roles.Duadelphe] = [{"self": "duadelphe", "perm": "rw"}]
	salons[Roles.Triadelphe] = [{"self": "triadelphe", "perm": "rw"}]

	for c in category.salonsDeJeu.keys():
		if c != "votes":
			await category.salonsDeJeu[c].set_permissions(j["personne"],overwrite=None)

	# Check si le rôle du/de la joueurse a des permissions particulière
	if j["role"] in salons.keys():
		salonName = salons[j["role"]]
		sal = []
		perm = {}
		message = {}
		
		perm["r"] = discord.PermissionOverwrite(read_messages=True,send_messages=None)
		message["r"] = j["personne"].mention + " tu peux ~~écr~~ lire ici."
		perm["rw"] = discord.PermissionOverwrite(read_messages=True,send_messages=True)
		message["rw"] = j["personne"].mention + " tu peux écrire ici !"
		for c in salonName:
			# remplace le nom du salon par l’objet de type Discord.Channel associé, parmi la liste des salons enregistrés
			await category.salonsDeJeu[c["self"]].set_permissions(j["personne"],overwrite=perm[c["perm"]])
			await category.salonsDeJeu[c["self"]].send(message[c["perm"]])
		if category.contamine_e:
			await category.salonsDeJeu["lg"].set_permissions(category.contamine_e,overwrite=perm["rw"])
	return

async def stop(room):
	try:
		category = getRoom(room)
		await GUI_EllipseJeu((0,0,0,255),"DejaVuSansMono.ttf",228,room)
		joueurses = category.stop(room)
		await room.send("STOP ! On arrête tout ! (J’en ai pour un petit moment :-) )")
		
		for j in joueurses:
			if j.voice:
				await j.edit(mute=False)
			for r in j.roles:
				if r.name == "Morts":
					await j.remove_roles(r)
			for chan in category.salonsDeJeu:
				await category.salonsDeJeu[chan].set_permissions(j, overwrite=None)

	except LGError:
		error_type, error_instance, tracebacka = sys.exc_info()
		await room.send(error_instance.args[0])
	
async def checkWin(room,elimine=None):
	category = getRoom(room)
	print("stillAlive : {} ; LG : {}".format(type(category.stillAlive()),type(category.lgs())))
	if not category.stillAlive() and category.pesteActivated() and self.mj == room.guild.me:
		await room.send("Tout le monde est mort. Et je suis la peste noire :3")
		await stop(room)
	elif not category.stillAlive() and category.pesteActivated():
		await room.send("Tout le monde est mort. Et læ MJ jouait la peste noire.")
		await stop(room)
	elif not category.stillAlive():
		await room.send("Tout le monde est mort. Niark")
		await stop(room)
	elif category.stillAlive() and category.lgs() and len(category.stillAlive()) == len(category.lgs()):
		await room.send("Les LG ont gagné !")
		await stop(room)
	elif not category.lgs() and not category.pesteActivated():
		await room.send("Le Village a gagné !")
		await stop(room)
	elif len(category.stillAlive()) == 2 and category.amoureuxes and category.amoureuxes[0] in category.stillAlive():
		await room.send("Les amoureuxes ont gagné !")
		await stop(room)
	elif elimine and category.nuitPassees <= 1 and category.getRole(elimine) == Roles.Ange:
		await room.send("L'ange a gagné !")
		await stop(room)
	else:
		return False
	raise LGError("Ok, j’ai fini d’arrêter la partie. Vous pouvez splash ou recommencer une partie.")

async def killOne(chan,elimine):
	category = getRoom(chan)
	mj = category.mj

	for r in chan.guild.roles:
		if r.name == "Morts":
			role = r

	if not role:
		await chan.send("Le rôles `Morts` n’existe pas sur ce serveur discord.")
		return

	if category.getMemberOfRole(Roles.Servant_e) and elimine not in category.getMemberOfRole(Roles.Servant_e):
		await chan.send("Servant_e, si tu souhaites prendre son rôle, dis `servir` au bout de 20 secondes, je considérerais que ce n'est pas le cas.")
		def killingOrNot(msg):
			if msg.author == mj and chan == msg.channel:
				return msg.content == "mort"
			elif msg.author in category.getMemberOfRole(Roles.Servant_e) and chan == msg.channel:
				return msg.content == "servir"
		try:
			msg = await client.wait_for("message",timeout=60,check=killingOrNot)
		except asyncio.TimeoutError:
			msg = None
	else:
		msg = None
	if msg and msg.content == "servir" and msg.author != elimine:
		await chan.send(elimine.display_name + " est mort" + str(emoji(chan.guild, "rip")))
		await chan.send(msg.author.display_name + " a pris son rôle")
		newRole = category.getRole(elimine)
		category.joueurses[msg.author] = newRole
		await msg.author.send("Ton nouveau rôle est : " + newRole.name)

		j = {"personne": msg.author, "role": newRole}
		if mj != msg.guild.me:
			await mj.send("{} : {}".format(j["personne"].display_name,j["role"].name))
			
		await changePerms(j, chan)
	 
	else:
		roleMort = category.getRole(elimine)
		if Variantes.Aura in category.variantes:
			if roleMort in CampLoupGarou.roles or elimine == category.contamine_e:
				await chan.send("{} était dans le camp des LG.".format(elimine))
			elif roleMort in CampVillageois_e.roles or roleMort == Roles.Ange and category.nuitPassees > 1:
				await chan.send("{} était dans le camp des Villageois_es.".format(elimine.display_name))
			else:
				await chan.send("{} était solitaire.".format(elimine.display_name))
		elif Variantes.Obscure in category.variantes:
			await chan.send("Le rôle de {} était : ||Lol t’y as cru||".format(elimine.display_name))
		else:
			await chan.send("Le rôle de {} était : {}".format(elimine.display_name,roleMort.name))
		if roleMort == Roles.Servant_e:
			await chan.send("ptdr t mor, ta servi à r déso.")
		elif roleMort == Roles.Chaman and category.nuitPassees == 1:
			await chan.send("chaman mor t1. Ptdr, aussi utile que les conseils des valides jpp.")
		elif roleMort == Roles.Chasseureuse:
			await chan.send("Ah ! Tu vas devoir tuer une personne. Dis juste son surnom")
			await eev(chan)
			def chassouKill(msg):
				who = category.getPersonBehindNick(msg.content)
				if msg.author == elimine and who and msg.channel == chan:
					return who
			msg = await client.wait_for("message",check=chassouKill)
			who = category.getPersonBehindNick(msg.content)
			print("killing {}".format(who.display_name))
			await killOne(chan,who)
		elif roleMort == Roles.Ancien_ne and category.ancienRevealed == False and category.mortParLesLoups == elimine:
			await chan.send("L'ancien n'a pas pu être tuer par les LG cette nuit, iel survivra un peu plus.")
			category.ancienRevealed = True
			return
		elif roleMort == Roles.Ancien_ne and (not chan in category.mortParLesLoups.keys() or not category.mortParLesLoups[chan] == elimine):
			await chan.send("Grossière erreur ! Les villageois·es perdent leurs pouvoirs !")
			category.powersActivated[chan] = False
		elif roleMort == Roles.Ancien_ne and category.ancienRevealed[chan]:
			await chan.send("Ah… Les LG se sont acharné·es.")
			
	await elimine.add_roles(role)
	if elimine == category.contamine_e:
		category.contamine_e = None
	await checkWin(chan,elimine)
	await chan.send(str(emoji(chan.guild, "rip")))
	
	amoureuxes = category.amoureuxes or None
	if amoureuxes and elimine in amoureuxes:
		amoureuxes.remove(elimine)
		await chan.send("Oh, tu étais amoureuxes… ")
		await chan.send("{} va aussi mourir.".format(amoureuxes[0].diplay_name))
		await killOne(chan,amoureuxes[0])

async def eev(room):
	try:
		category = getRoom(room)
		alive = category.stillAlive()
		astr = []
		for a in alive:
			astr.append("{} -> Surnom : `{}`".format(a.display_name,category.getNick(a)))
		astr.sort()
		await room.send("Voici la liste des personnes en vie et leur surnom :\n" + "\n".join(astr))

	except LGError:
		error_type, error_instance, tracebacka = sys.exc_info()
		await room.send("Message à {} : {}".format(msg.author,error_instance.args[0]))

async def jour(room):
	try:
		category = getRoom(room)
		mortsCetteNuit = category.jour()
		await checkWin(room)
		for ins in category.inscris:
			if ins.voice:
				print("unmutting", ins.display_name)
				await ins.edit(mute=False)
		await room.send("Le village se réveil")
		
		if mortsCetteNuit:
			random.shuffle(mortsCetteNuit)
			for m in mortsCetteNuit:
				await room.send(m.mention + " doit mourir.")
				await killOne(room,m)
		else:
			await room.send("Il n'y a pas de mort·es cette nuit (je crois ?).")
		await GUI_EllipseJeu((20,20,30,255),"DejaVuSansMono.ttf",228,room)
		await votes(room)
			
	except LGError:
		error_type, error_instance, tracebacka = sys.exc_info()
		await room.send("Message à {} : {}".format("ma développeuse",error_instance.args[0]))

async def votes(room,text=True):
	category = getRoom(room)
	salon = category.salonsDeJeu["votes"]
	if text:
		await salon.send("C'est le moment de voter. Dites simplement le surnom de la personne pour laquelle vous voulez voter. Vous pouvez débattre dans le salon village")
		await eev(salon)
	
	def who(msg):
		msgKeywords = keywords(msg)
		if msg.channel == salon and msg.author in category.stillAlive():
			return True
	msg = await client.wait_for("message",check=who)
	
	msgKeywords = keywords(msg)
	target = getM(msg,msgKeywords[0])
	
	try:
		if not target:
			await room.send("Vous devez mentionner une personne contre qui voter.")
			await votes(room,False)
			return

		retour = category.vote(salon,msg.author,target)
		await salon.send(retour)
		if category.votes:
			if len(category.stillAlive()) == len(category.votes):
				elimine = category.stopVotes()
				if elimine == category.PESTE and category.pesteActivated():
					await salon.send("Bien joué, il s'agissait bien de la peste noir ! Le village a gagné !")
					await stop(room)
					raise LGError("La partie est terminée")
				elif elimine == category.PESTE:
					await salon.send("**Raté**, ce n'est pas la Peste noir ! MJ, tu peux choisir une souffrance pour ce village")
					await nuit(room)
				else:
					await room.send(elimine.mention + " est éliminé·e par le village.")
					await killOne(room,elimine)
					await nuit(room)
			else:
				await votes(room,False)

	except LGError:
		error_type, error_instance, tracebacka = sys.exc_info()
		await salon.send("Message à {} : {}".format(msg.author,error_instance.args[0]))
	return

async def nuit(room):
	try:
		category = getRoom(room)
		if category.nuit():
			c = room
			if False: #À mettre en False pendant le développement et les phases de test
				for ins in category.inscris:
					if ins.voice:
						await ins.edit(mute=True)
			await c.send("Le village s’endort")
			print("duad")
			await duad(c)
			print("triad")
			await triad(c)
			print("cupi")
			await cupi(c)
			print("protect")
			await protect(c)
			print("spy")
			await spy(c)
			print("lycant")
			await lycant(c)
			print("infect")
			await infect(c)
			print("soso")
			await soso(c)
			print("jour")
			await jour(c)
	except LGError:
		error_type, error_instance, tracebacka = sys.exc_info()
		await room.send(error_instance.args[0])

def createWho(role,salon):
	category = getRoom(salon)
	def who(msg):
		msgKeywords = keywords(msg)
		if msg.channel == salon and msg.author in category.getMemberOfRole(role):
			return True
	return who

#Tx
async def duad(room):
	category = getRoom(room)
	if category.getMemberOfRole(Roles.Duadelphe):
		salon = category.salonsDeJeu["duadelphe"]
		await salon.send("Duadelphes, vous vous reconnaissez. Vous pourrez vous parler pendant la nuit uniquement, simplement vous savez l'innocence de votre adelphe. Si vous êtes enfant unique (pas d'autre Duadelphe), vous êtes comme un·e simple villageois·e.")
	return

#Tx
async def triad(room):
	category = getRoom(room)
	if category.getMemberOfRole(Roles.Triadelphe):
		salon = category.salonsDeJeu["triadelphe"]
		await salon.send("Tridelphes, vous vous reconnaissez. Vous pourrez vous parler pendant la nuit uniquement, simplement vous savez l'innocence de votre/vos adelphe·s. Si vous êtes enfant unique (pas d'autre Triadelphes), vous êtes comme un·e simple villageois·e.")
	return

#T1
async def cupi(room,text=True):
	category = getRoom(room)
	if not category.getMemberOfRole(Roles.Cupidon):
		await asyncio.sleep(random.randint(2,5))
		return
	if category.nuitPassees:
		return
	salon = category.salonsDeJeu["cupidon"]
	salonAm = category.salonsDeJeu["amoureuxes"]
	if text:
		await salon.send("Cupidon, tu peux désigner deux personnes qui seront amoureuses. Pour ça, dis juste leur surnom séparés d'un espace.")
	await eev(salon)
	try:
		who = createWho(Roles.Cupidon,salon)
		msg = await client.wait_for("message",timeout=180,check=who)
		msgKeywords = keywords(msg)
		if len(msgKeywords) < 2:
			await salon.send("Tu dois mentionner **deux** amoureuxes !")
			await cupi(room,text=False)
			return

		targets = [getM(msg,msgKeywords[0]),getM(msg,msgKeywords[1])]
	except asyncio.TimeoutError:
		await salon.send("Tu as mis trop de temps, je passe au rôle suivant")
		return
	try:
		if not targets or len(targets) < 2 or not targets[0] or not targets[1]:
			await salon.send("Tu dois donner le surnom de deux amoureuxes.")
			if targets and len(targets) == 2 and not targets[0] and not targets[1]:
				await salon.send("Je n'ai pas reconnu {}.".format(", ".join(msgKeywords[:1])))
			elif targets and len(targets) == 2 and not targets[0]:
				await salon.send("Je n'ai pas reconnu {}.".format(msgKeywords[0]))
			elif  targets and len(targets) == 2 and not targets[1]:
				await salon.send("Je n'ai pas reconnu {}.".format(msgKeywords[1]))
			await cupi(room,False)
			return

		amoureuxes = category.setAmoureuxes(msg.author, targets)
		await salonAm.set_permissions(amoureuxes[0], send_messages=True,read_messages=True)
		await salonAm.set_permissions(amoureuxes[1], send_messages=True,read_messages=True)
		await salon.send("{}  et {} sont maintenant amoureuxes.".format(targets[0].display_name,targets[1].display_name))
		await salonAm.send(amoureuxes[0].mention + " et " + amoureuxes[1].mention + ", vous êtes amoureuxes.")

	except LGError:
		error_type, error_instance, tracebacka = sys.exc_info()
		await salon.send(error_instance.args[0])
		await cupi(room,False)
	return

#Tx
async def protect(room,text=True):
	category = getRoom(room)
	if not category.getMemberOfRole(Roles.Protecteurice) and category.powersActivated:
		if category.powersActivated:
			await asyncio.sleep(random.randint(2,5))
		return
	salon = category.salonsDeJeu["protecteurice"]
	if text:
		await salon.send("Protecteurice, tu peux désigner une personne à protéger. Pour ça, dis juste son surnom.")
	await eev(salon)
	try:
		who = createWho(Roles.Protecteurice,salon)
		msg = await client.wait_for("message",timeout=180,check=who)
		target = getM(msg,keywords(msg)[0])
	except asyncio.TimeoutError:
		await salon.send("Tu as mis trop de temps, je passe au rôle suivant")
		return
	try:
		if not target:
			await salon.send("Vous devez mentionner une personne à protéger.")
			await protect(room,False)
			return

		prote = category.proteger(msg.author, target)
		await salon.send(prote.display_name + " est protégé.")

	except LGError:
		error_type, error_instance, tracebacka = sys.exc_info()
		await salon.send(error_instance.args[0])
		await protect(room,False)
	return

#Tx
async def spy(room,text=True):
	category = getRoom(room)
	if not category.getMemberOfRole(Roles.Espion_ne) and category.powersActivated:
		if category.powersActivated:
			await asyncio.sleep(random.randint(2,5))
		return
	salon = category.salonsDeJeu["espion_ne"]
	if text:
		await salon.send("Espion_ne, tu peux désigner une personne à scanner. Pour ça, dis juste son surnom.")
	await eev(salon)
	try:
		who = createWho(Roles.Espion_ne,salon)
		msg = await client.wait_for("message",timeout=180,check=who)
		target = getM(msg,keywords(msg)[0])
	except asyncio.TimeoutError:
		await salon.send("Tu as mis trop de temps, je passe au rôle suivant")
		return
	try:
		if not target:
			await salon.send("Vous devez mentionner une personne à scanner.")
			await spy(room,False)
			return

		scan = category.scan(msg.author, target)
		await salon.send("{} est {}.".format(target.display_name,scan))
		if Variantes.Bavard_e in category.variantes:
			await category.salonsDeJeu[Room.SALONS[0]].send("La personne espionnée cette nuit est {}.".format(scan))

	except LGError:
		error_type, error_instance, tracebacka = sys.exc_info()
		await salon.send(error_instance.args[0])
		await spy(room,False)
	return

#Tx
async def lycant(room,text=True):
	category = getRoom(room)
	salon = category.salonsDeJeu["lg"]
	if category.lgs() and text:
		await salon.send("LG, mettez-vous d'accord pour désigner une personne à manger. Dites successivement le surnom de la personne : [**ATTENTION** : Si vous dites que le surnom de la personne, le bot comprendra que vous voulez manger cette personne]")
	elif text:
		if category.mj == room.guild.me:
			await asyncio.sleep(random.randint(2,5))
			target = random.choice(category.stillAlive())
			msgKeywords = []
		else:
			await salon.send("Cher MJ, il n'y a pas de LG dans cette partie, la variante Peste Noire est donc active, tu peux tuer une personne comme si tu étais un·e LG. Il te suffit de dire son surnom.")
			
	await eev(salon)
	if "target" not in locals():
		try:
			def who(msg):
				msgKeywords = keywords(msg)
				if msg.channel == salon and (msg.author in category.lgs() or category.pesteActivated() and msg.author == category.mj):
					return True
			msg = await client.wait_for("message",timeout=180,check=who)
			msgKeywords = keywords(msg)
			target = getM(msg,msgKeywords[0])
		except asyncio.TimeoutError:
			await salon.send("Tu as mis trop de temps, je passe au rôle suivant")
			return
	try:
		if not target or len(msgKeywords) > 1:
			await lycant(room,False)
			return
		
		if msgKeywords:
			miam = category.miam(msg.author,target)
			await salon.send(miam)
		else:
			miam = category.miam(category.mj,target)
			await salon.send(miam)
			
		if len(category.lgs()) <= len(category.miams.keys()):
			await salon.send("Vous mangez {}".format(category.stopMiams().display_name))
		else:
			await lycant(room,False)

	except LGError:
		error_type, error_instance, tracebacka = sys.exc_info()
		await salon.send(error_instance.args[0])
		await lycant(room,False)
	return

#Tx
async def infect(room,text=True):
	category = getRoom(room)
	if not category.getMemberOfRole(Roles.InfectParentLoup):
		await asyncio.sleep(random.randint(2,5))
		return
	try:
		salon = category.salonsDeJeu["infect"]
		if category.mortParLesLoups != None and not category.contamine_e:
			if text:
				await salon.send("{} doit mourir.".format(category.mortParLesLoups.display_name))
				await salon.send("Souhaites-tu contaminer cette personnes à la place ? [Réponse : « oui » / « non »]")
				
			def contamineOrNot(msg):
				if msg.author in category.getMemberOfRole(Roles.InfectParentLoup) and msg.channel == salon:
					return msg.content.lower() in ["oui","ouais","ok","contaminer","contamine","non","oui.","non."]
			try:
				msg = await client.wait_for("message",timeout=180, check=contamineOrNot)
			except asyncio.TimeoutError:
				msg = None
			if msg and msg.content.lower() in ["oui","ouais","ok","contaminer","contamine","oui."]:
				category.contamine_e = category.mortParLesLoups
				category.mortsCetteNuit.remove(category.mortParLesLoups)
				category.mortParLesLoups = None
				await salon.send("Tu as contaminé cette pauvre personne.")
				await category.salonsDeJeu["lg"].set_permissions(category.contamine_e,send_messages=True,read_messages=True)
				await category.salonsDeJeu["lg"].send(category.contamine_e.mention + " tu as été contaminé·e par l'Infect Parent des LGs. Tu es désormais dans le camp des LG et dès le prochain tour, tu pourras jouer avec les lg, en plus de pouvoir utiliser tes pouvoirs habituels.")

			elif msg and msg.content.lower() in ["non","non."]:
				await salon.send("Ok, pas grave. Toute façon iel contait pas pour moi.")
			else:
				await salon.send("Ok, on va dire qu'on læ laisse mourir.")
		elif category.mortParLesLoups == None:
			await salon.send("Personne ne doit mourir cette nuit. Læ Protecteurice aurait protégé la personne ?")
	except LGError:
		error_type, error_instance, tracebacka = sys.exc_info()
		await infect(room,False)
		await salon.send(error_instance.args[0])
	return

#Tx
async def soso(room,text=True):
	category = getRoom(room)
	if not category.getMemberOfRole(Roles.Sorcier_e) and category.powersActivated:
		if category.powersActivated:
			await asyncio.sleep(random.randint(2,5))
		return
	try:
		salon = category.salonsDeJeu["sorcerie"]
		if category.mortParLesLoups != None and text:
			await salon.send(category.mortParLesLoups.display_name + " doit mourir.")
			if category.VIE in category.potions:
				await salon.send("Souhaites-tu sauver cette personnes  ? [Réponse : « oui » / « non »]")
				
				def saveOrNot(msg):
					if msg.author in category.getMemberOfRole(Roles.Sorcier_e) and msg.channel == salon:
						return msg.content.lower() in ["oui","ouais","ok","sauver","sauve","non","oui.","non."]
				try:
					msg = await client.wait_for("message",timeout=180, check=saveOrNot)
				except asyncio.TimeoutError:
					msg = None
				if msg and msg.content.lower() in ["oui","ouais","ok","sauver","sauve","oui."]:
					category.potionVie()
					await salon.send("Tu as sauvé cette pauvre personne, tu n'as plus de potion de vie.")
					category.potions.remove(category.VIE)

				elif msg and msg.content.lower() in ["non","non."]:
					await salon.send("Ok, pas grave. Toute façon iel contait pas pour moi.")
				else:
					await salon.send("Ok, on va dire qu'on læ laisse mourir.")
			else:
				await salon.send("Malheureusement, tu n'as plus de potion de vie.")
		elif text:
			await salon.send("Personne ne doit mourir cette nuit. Læ Protecteurice aurait protégé la personne ?")
		if category.MORT in category.potions:
			if text:
				await salon.send("Sorcier_e, tu peux désormais tuer une personne. Pour ça, dis juste son surnom. Si tu ne le souhaites pas, dis juste « non » ou « passer ».")
			await eev(salon)
			try:
				who = createWho(Roles.Sorcier_e,salon)
				msg = await client.wait_for("message",timeout=180,check=who)
				if msg.content.lower() in ["non","passer"]:
					await salon.send("Ok, next")
					return
			
				target = getM(msg,keywords(msg)[0])
			except asyncio.TimeoutError:
				await salon.send("Tu as mis trop de temps, je passe au rôle suivant")
				return
			
			try:
				if not target:
					await salon.send("Vous devez mentionner une personne à tuer.")
					await soso(room,False)
					return

				mort_e = category.potionMort(msg.author,target)
				await salon.send("Ok, noté (et tué·e).")
			except LGError:
				error_type, error_instance, tracebacka = sys.exc_info()
				await soso(room,False)
				await salon.send(error_instance.args[0])

	except LGError:
		error_type, error_instance, tracebacka = sys.exc_info()
		await salon.send(error_instance.args[0])
	return

@client.event
async def on_ready():
	print("* Bot {} logged successfully".format(client.user.name))

@client.event
async def on_member_remove(member):
	await channel(member.guild, ["accueil", "general"]).send("Au revoir {}".format(member.display_name))


@client.event
async def on_member_join(member):
	await channel(member.guild, ["accueil", "general"]).send("Bonjour et bienvenue à {}".format(member.mention))


@client.event
async def on_message(message):
	global mort
	try:
		if not message.guild:
			return

		#if message.author.bot:
		#	return

		prefix = "!"
		if message.content.find(prefix) != 0:
			return

		msgContent = message.content[len(prefix):].strip()
		msgKeywords = msgContent.split(" ")
		if len(msgKeywords) == 0:
			return

		cmd = msgKeywords[0].strip().lower()

		if cmd == "test":
			return

		elif cmd == "help":
			text = "**Commandes:**\n\n"
			text = text + "``" + prefix + "masteriser`` : Vous permet de masteriser une partie. Le bot vous **accompagne** dans **chaque étape**.\n"
			text = text + "Si vous voulez une **documentation** plus complète, je vous invite à aller directement sur le **site de la documentation** : <https://mdpad.powi.fr/9xlS9O9hTx2FQqzA7EiH8A?view>"
			await message.channel.send(text)
			return

		elif cmd in ["ls", "liste"]:
			category = getRoom(message.channel)
			await message.channel.send(category.existingRoles())
			return

		elif cmd == "wesh":
			await message.channel.send("Wesh les individus!!! ça va? Avec mes collègues on est chaud de rejoindre un groupe très mobile en vu de renverser l'état, de niquer les keufs et de commettre des dégradations! On se retrouve où? Vous auriez des photos de vous pour aider??? :p")
			return
			
		elif cmd == "shy":
			if len(msgKeywords) <= 1 or msgKeywords[1].lower() in ["default","yellow","y","simpson"]:
				messages = [" 👉👈 "," 👉  👈"]
			elif msgKeywords[1].lower() in ["light","l","blanc","white"]:
				messages = [" 👉🏻👈🏻 "," 👉🏻  👈🏻"]
			elif msgKeywords[1].lower() in ["ml","m-l","medium-light","mediumlight"]:
				messages = [" 👉🏼👈🏼 "," 👉🏼  👈🏼"]
			elif msgKeywords[1].lower() in ["m","medium"]:
				messages = [" 👉🏽👈🏽 "," 👉🏽  👈🏽"]
			elif msgKeywords[1].lower() in ["md","m-d","medium-dark","mediumdark"]:
				messages = [" 👉🏾👈🏾 "," 👉🏾  👈🏾"]
			elif msgKeywords[1].lower() in ["d","dark","noir","black"]:
				messages = [" 👉🏿👈🏿 "," 👉🏿  👈🏿"]
			else:
				messages = [" 👉👈 "," 👉  👈"]
			toMod = await message.channel.send(messages[1])
			for i in range(5):
				await toMod.edit(content=messages[i%len(messages)])
		
		elif cmd == "protect":
			await message.channel.send("Nymphali te protège, tout va bien se passer", file=discord.File("protect.gif"))
			return
			
		elif cmd == "seek":
			files = message.attachments or None
			if not files:
				await message.channel.send("Euh faut envoyer un fichier text")
				return
			
			for file in files:
				await file.save("temp.txt")
				f = open("temp.txt","r")
				try:
					lines = f.read().split("\n")
				except UnicodeDecodeError:
					await message.channel.send("Euh… Je préfère les fichiers texte")
					return
				toSend = []
				for r in lines:
					found = seek(r)
					if found:
						toSend.append("{} : \"{}\" (Trouvé : {})".format(lines.index(r),r,found))
				if toSend:
					f.close()
					f = open("temp.txt","w")
					f.write("\n".join(toSend))
					f.close()
				await message.channel.send(file=discord.File("temp.txt",filename=file.filename))
			
		elif cmd == "color":
			color = msgKeywords[1]
			if not color[0] == "#" or len(color) != 7:
				await message.channel.send("Merci d’envoyer une couleur au format #RRGGBB")
				return
			dColor = discord.Colour(int(color[1:],16))
			square = Image.new("RGB",(256,64),dColor.to_rgb())
			square.save("square.png","png")
			content = discord.Embed(color=dColor,title=color,description="Affichage de la couleur {} en Embed message.".format(color))
			await message.channel.send(embed=content,file=discord.File("square.png"))

		elif cmd == "jpp":
			await message.channel.send(file=discord.File("jpp.png"))
			return
			
		elif cmd == "purge":
			if message.author.guild_permissions.administrator:
				deleted = await message.channel.purge(limit=666,bulk=False)
				await message.channel.send("J’ai supprimé {} messages. [autodestruction au bout de 5 secondes]".format(len(deleted)),delete_after=5)
			else:
				await message.channel.send("Il faut avoir la permission d'administrer le serveur pour ça")
			return

		elif cmd == "paslà":
			if len(msgKeywords) <= 1:
				await message.channel.send("Vous n’êtes pas sur le bon salon pour ça.")
				return

			if msgKeywords[1] in ["nsfw", "NSFW", "aynnaysafedoublev"]:
				await message.channel.send("Merci d’aller dans le salon NSFW pour ce genre de choses qui n’a pas sa place ici.")

			elif msgKeywords[1] in ["niais", "niaiseries"]:
				await message.channel.send("AHHHHhhh… De l’amour entre des eukaryotes du règne animal. Partez en salon de niaiseries je vous pries.")

			elif msgKeywords[1] in ["nourriture", "food"]:
				await message.channel.send("Certaines personnes sont mal à l’aise avec la nourriture… Comprends qu’il y a des personnes qui ont des TCA… STP…")

			elif msgKeywords[1] in ["cis", "het", "cismec", "cishet", "cismechet"]:
				await message.channel.send("Hein ? Pas de ça ici s’il vous plaît…")

			elif msgKeywords[1] in ["©", "copyright", "®", "™"]:
				await message.channel.send("KEU-WA ? KEU-WA ? KEU-WA ? AHHHHhhhhhh.")
				await asyncio.sleep(1)
				await message.channel.send("KEU-WA ?")
				await asyncio.sleep(5)
				await message.channel.send("KEU-WA ? Du droit d’auteur ici ??")
				await asyncio.sleep(5)
				await message.channel.send("KEU-WA ?")
				await asyncio.sleep(5)
				await message.channel.send("Blllllbblbllllll… Étrange sensation. Vive la gauche d’auteur !")

			elif msgKeywords[1] in ["caps", "cops", "CAPS", "majuscule", "MAJUSCULE", "majuscules", "MAJUSCULES"]:
				await message.channel.send("Les caps, c’est comme les cops. On n’en veut pas ici.")
			elif msgKeywords[1] in ["coronavirus", "nCor-19", "CoVid-19", "19-nCor", "SRAS", "Coronavirus"]:
				await message.channel.send("Hey, le sujet peut être très anxiogène pour beaucoup de monde, s'il te plaît, garde ce sujet pour le salon <#687975916932169738>. Pour y avoir accès, demande le rôle `cvd` à un·e Modo Visible (tu peux les taguer). Sur ce discord, il y a pas mal de personnes sujetes à l'anxiété, merci de penser à elles.")

			else:
				await message.channel.send("Alors… Euh… J’ai pas compris. Mais en gros faut pas être ici parce que ce que vous dites est a priori en rapport avec : `" + msgKeywords[1] + "`")

		elif cmd == "v":
			if not message.author.voice:
				await message.channel.send("Vous devez lancer cette commande depuis un salon vocal.")
				return
				
			name = message.channel.name
			voiceChan = message.author.voice.channel
			if len(msgKeywords) > 1:
				name = " ".join(msgKeywords[1:])
			else:			
				name = name.replace("-"," ")
				name = name.replace("_"," ")
				name = name.title()
			
			await message.channel.send("Vous êtes dans le salon actuellement nommé « {} » ; Son ID étant : {}, je vais le renommer en : « {} »… Bon ça prend des fois du temps, donc je te préviendrais d’ici-là.".format(voiceChan.name,voiceChan.id,name))
			try:
				await voiceChan.edit(name=name)
			except HTTPException:
				await message.channel.send("Ahhhh ça pas marche. Genre Erreur HTTP je sais pas quoi, je parle pas ça moi, moi j'parle python")
			except:
				await message.channel.send("Alors il y a bien une erreur, mais alors attends, je vais t'envoyer le traceback, tu vas voir flou :")
				errmessage = "Message :\n```markdown\n" + prefix
				if "msgKeywords" in locals() or "msgKeywords" in globals():
					errmessage += " ".join(msgKeywords[0:]) + "```\n"
				errmessage += "Salon : " + message.channel.mention + "\n"
				errmessage += "Auteurice :" + message.author.display_name + "\n"
				errmessage += "Error message : ```python\n" + traceback.format_exc() + "```"
				await errlog(message).send(errmessage)
				print(traceback.format_exc())
			
			await message.channel.send("{} ouais c'est bon, j'ai renommé le salon. ’fin j'ai demandé à Discord, si ça prend trois heure, c'est sur Discord qu'il faut taper, pas sur moi, stp ♥.".format(message.author.mention))
		
		elif cmd == "fleurs":
			flowers = "🌹🌷💐🌺🌼"
			picked = "".join(random.choices(flowers,k=random.randint(2,20)))
			await message.channel.send(picked)
		
		elif cmd == "nsfw":
			nsfw = message.channel.is_nsfw()
			if nsfw:
				await message.channel.send("Oui")
			else:
				await message.channel.send("Non")
        
		elif cmd == "blague":
			blagues = []
			question = 0
			reponse = 1
			blagues.append(["Combien faut-il de véganes pour changer une ampoule ?","Il n'y a pas d'ampoule sur l'île déserte"])
			blagues.append(["Vous savez pourquoi Macron a été élu Président ?","Ah… Moi non plus."])
			blagues.append(["[Food] Que peut-on faire avec 3kg de beurre et 3kg de sucre ?","Une portion de Kouign-Amann"])
			blagues.append(["C'est de ne jamais arriver dans le bon ordre.","Tu sais quel est le plus gros défaut des paquets UDP ?"])
			blagues.append(["Comment appelle-t-on un psychanalyste qui soigne vraiment les gens ?","https://youtu.be/dQw4w9WgXcQ"])
			blagues.append(["Comment appelle-t-on un flic raciste, sexiste, validiste et homophobe ?","En composant le 17"])
			
			blague = random.choice(blagues)
			await message.channel.send(blague[question])
			def rep(msg):
				if msg.channel == message.channel:
					return True
			try:
				await client.wait_for("message",timeout=20,check=rep)
			except asyncio.TimeoutError:
				pass
			await message.channel.send(blague[reponse])

		elif cmd in ["mj", "je_suis_mj"] or len(msgKeywords) >= 3 and "_".join(msgKeywords[0:3]) == "je_suis_mj":
			try:
				category = getRoom(message.channel)
				mj = category.setMJ(message.author)
				await message.channel.send(mj.mention + " est nôtre MJ désormais.")
				
			except LGError:
				error_type, error_instance, tracebacka = sys.exc_info()
				await message.channel.send(error_instance.args[0])

		elif cmd in ["roles", "rôles"]:
			try:
				category = getRoom(message.channel)
				category.setPool(message.channel,message.author, "".join(msgKeywords[1:]).lower())
				await message.channel.send("Rôles sélectionnés !")

			except LGError:
				error_type, error_instance, tracebacka = sys.exc_info()
				await message.channel.send(error_instance.args[0])
		
		elif cmd in ["variantes", "variante", "mod", "mods"]:
			try:
				category = getRoom(message.channel)
				category.setMods(message.channel,message.author, "".join(msgKeywords[1:]).lower())
				await message.channel.send("Variantes ajoutées !")
				
			except LGError:
				error_type, error_instance, tracebacka = sys.exc_info()
				await message.channel.send(error_instance.args[0])

		elif cmd in ["spectateurice", "specta"]:
			try:
				category = getRoom(message.channel)
				spectateurice = False
				for r in message.guild.roles:
					if r.name == "Spectateurice":
						spectateurice = r
				if (not spectateurice):
					await message.channel.send("Le rôle n’existe pas sur ce serveur")
					return
				for chan in message.guild.channels:
					if chan.name == "village":
						if message.author in category.inscris:
							await message.channel.send("Vous ne pouvez pas être spectateurice si vous êtes dans une partie (tricheurse !)")
							return
				await client.add_roles(message.author, spectateurice)
				await message.channel.send("Vous êtes désormais Spectateurice sur ce serveur")

			except LGError:
				error_type, error_instance, tracebacka = sys.exc_info()
				await message.channel.send(error_instance.args[0])

		elif cmd in ["inscription", "ins"]:
			try:
				category = getRoom(message.channel)
				spectateurice = False
				for r in message.guild.roles:
					if r.name == "Spectateurice":
						spectateurice = r
				for mention in msgKeywords[1:]:
					try:
						inscris = []
						m = re.search('<@!?([0-9]*)>', mention)
						if m:
							member = message.guild.get_member(int(m.group(1)))
							print(member.name)
							if member:
								inscris = category.inscrire(message.channel,member,message.author)
								spectateurice and await message.author.remove_roles(spectateurice)
							else:
								await message.channel.send("Impossible de trouver la personne " + member.name + " mentionnée dans ce serveur.")
						else:
							print(mention)
							await message.channel.send("Vous devez mentionner une personne à inscrire." + mention + " n’est pas reconnue.")
					except TabError:
						error_type, error_instance, tracebacka = sys.exc_info()
						await message.channel.send(error_instance.args[0])
				if len(msgKeywords) == 1:
					cat = getRoom(message.channel)
					inscris = category.inscrire(message.channel,message.author)
					spectateurice and await message.author.remove_roles(spectateurice)
				tab = []
				for i in category.inscris:
					tab.append(i.display_name)
				await message.channel.send("{} Inscrit·e·s : {}".format(len(tab),", ".join(tab)))

			except LGError:
				error_type, error_instance, tracebacka = sys.exc_info()
				await message.channel.send(error_instance.args[0])

		elif cmd in ["des", "desinscription", "désinscription"]:
			try:
				category = getRoom(message.channel)
				for mention in msgKeywords[1:]:
					m = re.search('<@!?([0-9]*)>', mention)
					if m:
						member = message.guild.get_member(int(m.group(1)))
						if member:
							inscris = category.desinscrire(message.channel,member, message.author)
						else:
							await message.channel.send("Impossible de trouver la personne " + member.name + " mentionnée dans ce serveur.")
					else:
						await message.channel.send("Vous devez mentionner une personne à désinscrire." + m + " n’est pas reconnue.")
				if len(msgKeywords) == 1:
					inscris = category.desinscrire(message.channel,message.author)

				tab = []
				for i in inscris:
					tab.append(i.display_name)
				await message.channel.send("{} inscrit·e·s : {}".format(len(tab),", ".join(tab)))

			except LGError:
				error_type, error_instance, tracebacka = sys.exc_info()
				await message.channel.send(error_instance.args[0])

		elif cmd in ["lch", "lancer", "start"]:
			try:
				category = getRoom(message.channel)
				salon = message.channel
				await salon.send("Je prépare la partie, j’en ai pour un petit moment.")
				if not category.mj:
					category.setMJ(salon.guild.me)
					await salon.send("Ok, je serais MJ")
				
				for c in category.salonsDeJeu.keys():
					chan = category.salonsDeJeu[c]
					for o in chan.overwrites.keys():
						if type(o) == discord.member.Member:
							await chan.set_permissions(o,overwrite=None)
				joueurses = category.start(message.channel,message.author)
				
				spectateurice = False
				for r in message.guild.roles:
					if r.name == "Spectateurice":
						spectateurice = r

				perm_rw = discord.PermissionOverwrite(send_messages=True, read_messages=True)
				perm_r = discord.PermissionOverwrite(read_messages=True)

				for j in joueurses:
					await j["personne"].send("Votre rôle est : {} et votre surnom est : {}".format(j["role"].name,category.getNick(j["personne"])))
					if category.isDead(j["personne"]):
						r_mort = category.roleMort
						await j["personne"].remove_roles(r_mort)
					if category.mj != message.guild.me:
						await category.mj.send(j["personne"].display_name + " : " + j["role"].name + " ; Surnom : " + category.getNick(j["personne"]))

					await changePerms(j, message.channel)
				if len(category.getMemberOfRole(Roles.Chaman)) == 0 and spectateurice:
					mort = category.salonsDeJeu["morts"]
					if not mort:
						await message.channel.send("Le salon `#morts` n’existe pas sur ce serveur")
					else:
						spectateurice or await mort.set_permissions(spectateurice, overwrite=perm_rw)

				await message.channel.send("C’est parti pour une partie ! Vous n’êtes pas parti·es ?")
				await GUI_EllipseJeu((20,20,30,255),"DejaVuSansMono.ttf",228,salon)
				await nuit(message.channel)
			except LGError:
				error_type, error_instance, tracebacka = sys.exc_info()
				await message.channel.send(error_instance.args[0])

		elif cmd in ["eev", "still_alive", "encore_en_vie"] or len(msgKeywords) >= 2 and "_".join(msgKeywords[0:]) in [
			"still_alive", "encore_en_vie"]:
			try:
				category = getRoom(message.channel)
				alive = category.stillAlive()
				astr = []
				for a in alive:
					astr.append(a.display_name + " -> Surnom : `" + category.getNick(a) + "`")
				astr.sort()
				await message.channel.send("Personnes encore en vie :\n" + "\n".join(astr))

			except LGError:
				error_type, error_instance, tracebacka = sys.exc_info()
				await message.channel.send(error_instance.args[0])

		elif cmd == "checkrole":
			try:
				category = getRoom(message.channel)
				if len(msgKeywords) <= 1:
					return

				m = getM(message,msgKeywords[1])
				if m:
					if m:
						await message.channel.send("Le rôle de {} était : {}".format(m.display_name,category.getRole(message.channel,message.author, m).name))
					else:
						await message.channel.send("Impossible de trouver la personne mentionnée dans ce serveur.")
				else:
					print(msgKeywords[1])
					await message.channel.send("Vous devez mentionner une personne de qui regarder le rôle.")
			except LGError:
				error_type, error_instance, tracebacka = sys.exc_info()
				await message.channel.send(error_instance.args[0])

		elif cmd in ["stp", "arreter", "arrêter", "stop"] and len(msgKeywords) <= 1:
			try:
				category = getRoom(message.channel)
				if not category.partieLancee:
					await message.channel("Ok, j'arrête aucune partie. Voilà, c'est fait… Si on peut considérer que j'ai fais quelque chose")
					return
				await stop(message.channel)
				
			except LGError:
				error_type, error_instance, tracebacka = sys.exc_info()
				await message.channel.send(error_instance.args[0])
				
		elif cmd in ["splash", "reset"]:
			try:
				category = getRoom(message.channel)
				if len(msgKeywords) > 1:
					await message.channel.send(await category.splash(message.author, msgKeywords[1]))
				else:
					await message.channel.send(await category.splash(message.author))
			except LGError:
				error_type, error_instance, tracebacka = sys.exc_info()
				await message.channel.send(error_instance.args[0])

		elif cmd in ["credit", "crédit"]:
			g = message.guild
			testeurses = [292721793477771265,302173705738387456,310400799635406849,178081096238366720,429016801184317440,
						  230018511366258688,164778957948846080,234627286253436928,366633900455362560]
			t_names = ""
			for k in testeurses:
				t_names = "{}\n·{}".format(t_names,getName(g,k))
			
			dons = [-1]
			d_names = ""
			for k in dons:
				d_names = "{}\n·{}".format(d_names,getName(g,k))
			
			codeurses = {203135242813440001:"Code principal",378996246427336704:"Génération d’images"}
			c_lignes = ""
			for k in codeurses.keys():
				c_lignes = "{}\n·{} [{}]".format(c_lignes,getName(g,k),codeurses[k])
			
			autre = {95607371132313600:"Aide au code",310400799635406849:"Synthétisation des suggestions, Documentation, orthographe,…",
					 302173705738387456:"Orthographe",292721793477771265:"Orthographe, Débuggage intensif"}
			a_lignes = ""
			for k in autre.keys():
				a_lignes = "{}\n·{} [{}]".format(a_lignes,getName(g,k),autre[k])
			credit = """Je m’appelle Loup Garou ou Louve Garou ou… Peu importe. Je suis un·e bot responsable ici de la gestion des parties de Loup Garou de la Baie des Pirates.\nJe suis codée ~~[uniquement]~~ principalement par Powi qui passe beaucoup d’heures à coder :-)\
			\nSi vous voulez contribuer au code source, vous pouvez rejoindre gitlab 🙂 : <https://framagit.org/FirePowi/BotDiscordLG.py>\
			\nListe des contributeurices :\
			\n\n**Code source** :{}\
			\n\n**Dons** <https://www.paypal.me/PowiDenis> ou <https://patreon.com/powi> :{}\n\
			\n**Testeureuses :**\
			\n*l'air de rien, sans ces personnes, le développement ne pourrait pas avancer*{}\n\
			\n**Autres contributions ô combien importantes :**{}""".format(c_lignes,d_names,t_names,a_lignes)
			await message.channel.send(credit)

		elif cmd in ["source", "git", "code"]:
			await message.channel.send("<https://framagit.org/FirePowi/BotDiscordLG.py>")
			
		elif cmd == "profil":
			try:
				for mention in msgKeywords[1:]:
					try:
						inscris = []
						m = re.search('<@!?([0-9]*)>', mention)
						if m:
							member = message.guild.get_member(int(m.group(1)))
							if member:
								await message.channel.send(member.avatar_url_as(format="png",size=128))
							else:
								await message.channel.send("Impossible de trouver la personne " + member.name + " mentionnée dans ce serveur.")
						else:
							print(mention)
							await message.channel.send("Vous devez mentionner une personne à inscrire." + mention + " n’est pas reconnue.")
					except TabError:
						error_type, error_instance, tracebacka = sys.exc_info()
						await message.channel.send(error_instance.args[0])
				if len(msgKeywords) == 1:
					await message.channel.send("Voici l'url de l'avatar de {} (ID :{}) : {}".format(message.author.mention,message.author.id,message.author.avatar_url_as(format="png",size=128)))

			except LGError:
				error_type, error_instance, tracebacka = sys.exc_info()
				await message.channel.send(error_instance.args[0])

		else:
			await message.channel.send("Ouais, c’est pas faux.")

	except:
		await message.channel.send("Oups... I did it again.")
		errmessage = "ERROR :\n"
		errmessage += "Message :\n```markdown\n" + prefix
		if "msgKeywords" in locals() or "msgKeywords" in globals():
			errmessage += " ".join(msgKeywords[0:]) + "```\n"
		errmessage += "Salon : " + message.channel.mention + "\n"
		errmessage += "Auteurice :" + message.author.display_name + "\n"
		errmessage += "Error message : ```python\n" + traceback.format_exc() + "```"
		await errlog(message).send(errmessage)
		print(traceback.format_exc())

if __name__ == '__main__':
	global C
	C = Const()
	# CONSTANTES
	C.JOUR = 0
	C.NUIT = 1

	C.UNUSED = 0
	C.USED = 1
	# Init Discord client
	if len(sys.argv) < 1:
		print("Usage: " + sys.argv[0] + " <DISCORD_TOKEN>")
		exit(0)

	discord_token = sys.argv[1]
	global rooms

	rooms = {}
	client.run(discord_token)